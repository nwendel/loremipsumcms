﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Model;
using Xunit;

namespace LoremIpsumCMS.Tests.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class PageOverviewTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreate()
        {
            var tested = new PageOverview("Page/1", "Site/1", "/some-path", "Some Title", "System.String");

            Assert.Equal("Page/1", tested.PageId);
            Assert.Equal("Site/1", tested.SiteId);
            Assert.Equal("/some-path", tested.Path);
            Assert.Equal("Some Title", tested.Title);
            Assert.Equal("System.String", tested.DataTypeFullName);
            Assert.Equal("Site/1//some-path", tested.SiteIdAndPath);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanUpdate()
        {
            var tested = new PageOverview("Page/1", "Site/1", "/some-path", "Some Title", "System.String");
            tested.Update("/another-path", "Another Title");

            Assert.Equal("Page/1", tested.PageId);
            Assert.Equal("Site/1", tested.SiteId);
            Assert.Equal("/another-path", tested.Path);
            Assert.Equal("Another Title", tested.Title);
            Assert.Equal("System.String", tested.DataTypeFullName);
            Assert.Equal("Site/1//another-path", tested.SiteIdAndPath);
        }

    }

}

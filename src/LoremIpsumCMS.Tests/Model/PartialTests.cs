﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Tests.Model.Classes;

using Xunit;

namespace LoremIpsumCMS.Tests.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class PartialTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanConstruct()
        {
            var tested = new Partial();

            Assert.NotNull(tested.ExtensionProperties);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreate()
        {
            var tested = new Partial();
            var expectedData = new SomePartialData();
            var expectedCreatedAt = DateTime.Now;
            tested.ApplyEvent(new PartialCreatedEvent
            {
                SiteId = "Site/1",
                Name = "Some Partial",
                Data = expectedData,
                ExtensionProperties = new AbstractPartialExtensionProperties[0],
                CreatedAt = expectedCreatedAt
            });

            Assert.Equal("Site/1", tested.SiteId);
            Assert.Equal("Some Partial", tested.Name);
            Assert.Same(expectedData, tested.Data);
            Assert.NotNull(tested.ExtensionProperties);
            Assert.Equal(expectedCreatedAt, tested.CreatedAt);
            Assert.Null(tested.LastUpdatedAt);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanUpdate()
        {
            var tested = new Partial();
            var expectedData = new SomePartialData();
            var expectedUpdatedAt = DateTime.Now;
            tested.ApplyEvent(new PartialUpdatedEvent
            {
                Name = "Some Partial",
                Data = expectedData,
                ExtensionProperties = new AbstractPartialExtensionProperties[0],
                UpdatedAt = expectedUpdatedAt
            });

            Assert.Null(tested.SiteId);
            Assert.Equal("Some Partial", tested.Name);
            Assert.Same(expectedData, tested.Data);
            Assert.NotNull(tested.ExtensionProperties);
            Assert.Equal(default(DateTime), tested.CreatedAt);
            Assert.True(tested.LastUpdatedAt.HasValue);
            Assert.Equal(expectedUpdatedAt, tested.LastUpdatedAt);
        }

    }

}

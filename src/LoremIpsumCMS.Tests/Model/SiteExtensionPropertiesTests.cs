﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.ObjectModel;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Tests.Model.Classes;

using Xunit;

namespace LoremIpsumCMS.Tests.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class SiteExtensionPropertiesTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetDynamicPropertyTypeGeneric()
        {
            var tested = new Site();
            var expected = new SomeSiteExtensionProperties();

            tested.ApplyEvent(new SiteUpdatedEvent
            {
                ExtensionProperties = new Collection<AbstractSiteExtensionProperties> { expected }
            });

            var result = tested.GetExtensionProperty<SomeSiteExtensionProperties>();

            Assert.Same(expected, result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetDynamicPropertyType()
        {
            var tested = new Site();
            var expected = new SomeSiteExtensionProperties();

            tested.ApplyEvent(new SiteUpdatedEvent
            {
                ExtensionProperties = new Collection<AbstractSiteExtensionProperties> { expected }
            });

            var result = tested.GetExtensionProperty(typeof(SomeSiteExtensionProperties));

            Assert.Same(expected, result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetGenericPropertyByGenericTypeDefault()
        {
            var tested = new Site();
            var result = tested.GetExtensionProperty<SomeSiteExtensionProperties>();

            Assert.NotNull(result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetGenericPropertyByTypeDefault()
        {
            var tested = new Site();
            var result = tested.GetExtensionProperty(typeof(SomeSiteExtensionProperties));

            Assert.NotNull(result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanSetAndGetString()
        {
            var tested = new Site();
            tested.ApplyEvent(new SiteUpdatedEvent
            {
                ExtensionProperties = new Collection<AbstractSiteExtensionProperties>
                {
                    new SomeSiteExtensionProperties { SomeStringValue = "The value" }
                }
            });

            var value = tested.GetExtensionProperty<SomeSiteExtensionProperties>(x => x.SomeStringValue);
            Assert.Equal("The value", value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanSetAndGetInt()
        {
            var tested = new Site();
            tested.ApplyEvent(new SiteUpdatedEvent
            {
                ExtensionProperties = new Collection<AbstractSiteExtensionProperties>
                {
                    new SomeSiteExtensionProperties { SomeBoolValue = true }
                }
            });

            var value = tested.GetExtensionProperty<SomeSiteExtensionProperties>(x => x.SomeBoolValue);
            Assert.Equal(true, value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanSetAndGetBool()
        {
            var tested = new Site();
            tested.ApplyEvent(new SiteUpdatedEvent
            {
                ExtensionProperties = new Collection<AbstractSiteExtensionProperties>
                {
                    new SomeSiteExtensionProperties { SomeIntValue = 2 }
                }
            });

            var value = tested.GetExtensionProperty<SomeSiteExtensionProperties>(x => x.SomeIntValue);
            Assert.Equal(2, value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetDefaultValue()
        {
            var tested = new Site();
            var value = tested.GetExtensionProperty<SomeSiteExtensionProperties>(x => x.SomeStringValue);

            Assert.Null(value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOn()
        {
            var tested = new Site();
            Assert.Throws<ArgumentException>(() => tested.GetExtensionProperty<SomePageExtensionProperties>(x => x.SomeStringValue));
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
namespace LoremIpsumCMS.Tests.Infrastructure.Proxy.TestClasses
{

    /// <summary>
    /// 
    /// </summary>
    public class GenericFunctionWIthArguments : Empty, IGenericFunctionWIthArguments
    {

        /// <summary>
        /// 
        /// </summary>
        public bool Invoked { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="instance"></param>
        /// <returns></returns>
        public T Operation<T>(T instance)
            where T : Empty, IGenericFunctionWIthArguments, new()
        {
            Invoked = true;
            return instance;
        }

    }

}

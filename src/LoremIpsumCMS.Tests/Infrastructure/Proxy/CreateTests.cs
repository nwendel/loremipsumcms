﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Reflection;
using LoremIpsumCMS.Infrastructure.Proxy;
using LoremIpsumCMS.Tests.Infrastructure.Proxy.TestClasses;

using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Proxy
{

    /// <summary>
    /// 
    /// </summary>
    public class CreateTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateEmptyInterface()
        {
            var target = new Empty();
            var tested = ProxyFactory.Create<IEmpty>(() => target);
            var proxiedTarget = (tested as IProxyTarget<IEmpty>)?.Target;

            Assert.Same(target, proxiedTarget);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateProcedureNoArgumentsInterface()
        {
            var target = new ProcedureNoArguments();
            var tested = ProxyFactory.Create<IProcerdureNoArguments>(() => target);
            tested.Operation();
            var proxiedTarget = (tested as IProxyTarget<IProcerdureNoArguments>)?.Target;

            Assert.Same(target, proxiedTarget);
            Assert.True(target.Invoked);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateFunctionNoArgumentsInterface()
        {
            var target = new FunctionNoArguments();
            var tested = ProxyFactory.Create<IFunctionNoArguments>(() => target);
            var result = tested.Operation();
            var proxiedTarget = (tested as IProxyTarget<IFunctionNoArguments>)?.Target;

            Assert.Same(target, proxiedTarget);
            Assert.True(target.Invoked);
            Assert.Equal(42, result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateProcedureWithArgumentsInterface()
        {
            var target = new ProcedureWithArguments();
            var tested = ProxyFactory.Create<IProcedureWithArguments>(() => target);
            tested.Operation(new object(), 42, true, "adsf", null);
            var proxiedTarget = (tested as IProxyTarget<IProcedureWithArguments>)?.Target;

            Assert.Same(target, proxiedTarget);
            Assert.True(target.Invoked);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateGenericFunctionWithArgumentsInterface()
        {
            var target = new GenericFunctionWIthArguments();
            var tested = ProxyFactory.Create<IGenericFunctionWIthArguments>(() => target);
            var result = tested.Operation(target);
            var proxiedTarget = (tested as IProxyTarget<IGenericFunctionWIthArguments>)?.Target;

            Assert.Same(target, proxiedTarget);
            Assert.True(target.Invoked);
            Assert.Equal(target, result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateInheritedInterfaces()
        {
            var tested = ProxyFactory.CreateType<IChildInterface>();

            Assert.Equal(4, tested.GetTypeInfo().GetInterfaces().Length);
            Assert.Contains(typeof(IProxyTarget<IChildInterface>), tested.GetTypeInfo().GetInterfaces());
            Assert.Contains(typeof(IChildInterface), tested.GetTypeInfo().GetInterfaces());
            Assert.Contains(typeof(IParentInterface), tested.GetTypeInfo().GetInterfaces());
            Assert.Contains(typeof(IParentParentInterface), tested.GetTypeInfo().GetInterfaces());
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using LoremIpsumCMS.Infrastructure.Validation2;
using LoremIpsumCMS.Tests.Infrastructure.Validation2.Classes;

using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class LengthValidationTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateLength()
        {
            var tested = new CustomerNameLengthValidator();
            var customer = new Customer { Name = "Qwerty" };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateLengthNull()
        {
            var tested = new CustomerNameLengthValidator();
            var customer = new Customer { Name = null };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateLengthTooShort()
        {
            var tested = new CustomerNameLengthValidator();
            var customer = new Customer { Name = "Asd" };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Equal(1, messages.Count);
            Assert.All(messages, x =>
            {
                Assert.Equal("Name", x.PropertyName);
                Assert.Equal("Minimum allowed length for Name is 4", x.Text);
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateLengthTooLong()
        {
            var tested = new CustomerNameLengthValidator();
            var customer = new Customer { Name = "1234567" };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Equal(1, messages.Count);
            Assert.All(messages, x =>
            {
                Assert.Equal("Name", x.PropertyName);
                Assert.Equal("Maximum allowed length for Name is 6", x.Text);
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateExactLength()
        {
            var tested = new CustomerNameExactLengthValidator();
            var customer = new Customer { Name = "Qwerty" };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateExactLengthNull()
        {
            var tested = new CustomerNameExactLengthValidator();
            var customer = new Customer { Name = null };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateExactLengthIncorrect()
        {
            var tested = new CustomerNameExactLengthValidator();
            var customer = new Customer { Name = "Asdf" };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Equal(1, messages.Count);
            Assert.All(messages, x =>
            {
                Assert.Equal("Name", x.PropertyName);
                Assert.Equal("Length for Name must be 6", x.Text);
            });
        }

        #region Validators

        /// <summary>
        /// 
        /// </summary>
        public class CustomerNameLengthValidator : AbstractValidator<Customer>
        {

            /// <summary>
            /// 
            /// </summary>
            public CustomerNameLengthValidator()
            {
                Property(x => x.Name).Length(4, 6);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public class CustomerNameExactLengthValidator : AbstractValidator<Customer>
        {

            /// <summary>
            /// 
            /// </summary>
            public CustomerNameExactLengthValidator()
            {
                Property(x => x.Name).Length(6);
            }

        }

        #endregion

    }

}

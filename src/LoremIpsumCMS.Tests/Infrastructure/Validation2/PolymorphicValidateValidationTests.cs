﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using FluentRegistration;
using LoremIpsumCMS.Infrastructure.Validation2;
using LoremIpsumCMS.Tests.Infrastructure.Validation2.Classes;

using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Validation2
{

    /// <summary>
    /// 
    /// </summary>
    public class PolymorphicValidateValidationTests
    {

        private readonly IServiceProvider _serviceProvider;

        /// <summary>
        /// 
        /// </summary>
        public PolymorphicValidateValidationTests()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.Register(AllClasses
                .FromTypes(new[] { typeof(AddressContactValidator), typeof(PhoneContactValidator) })
                .WithService.AllInterfaces());
            _serviceProvider = serviceCollection.BuildServiceProvider();
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidatePolymorphic()
        {
            var tested = new CustomerDefaultContactValidator();
            var customer = new Customer
            {
                Contacts = new AbstractContact[]
                {
                    new AddressContact()
                }
            };
            var context = new ValidationContext<Customer>(customer, _serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Equal(2, messages.Count);
            Assert.Contains(messages, x => x.PropertyName == "DefaultContact.Line1");
            Assert.Contains(messages, x => x.Text == "Line1 must not be null");
            Assert.Contains(messages, x => x.PropertyName == "DefaultContact.Line2");
            Assert.Contains(messages, x => x.Text == "Line2 must not be null");
        }

        ///// <summary>
        ///// 
        ///// </summary>
        //[Fact]
        //public void CanValidateListPolymorphic()
        //{
        //    var tested = new CustomerContactsValidator();
        //    var customer = new Customer
        //    {
        //        Contacts = new AbstractContact[]
        //        {
        //            new AddressContact(),
        //            new PhoneContact()
        //        }
        //    };
        //    var context = new ValidationContext<Customer>(customer, _serviceProvider);
        //    var messages = tested.Validate(context).ToList();

        //    Assert.Equal(3, messages.Count);
        //    Assert.Contains(messages, x => x.PropertyName == "Contacts[0].Line1");
        //    Assert.Contains(messages, x => x.Text == "Line1 must not be null");
        //    Assert.Contains(messages, x => x.PropertyName == "Contacts[0].Line2");
        //    Assert.Contains(messages, x => x.Text == "Line2 must not be null");
        //    Assert.Contains(messages, x => x.PropertyName == "Contacts[1].Number");
        //    Assert.Contains(messages, x => x.Text == "Number must not be null");
        //}

        #region Validators

        /// <summary>
        /// 
        /// </summary>
        public class CustomerDefaultContactValidator : AbstractValidator<Customer>
        {

            /// <summary>
            /// 
            /// </summary>
            public CustomerDefaultContactValidator()
            {
                Property(x => x.DefaultContact).PolymorphicValidate();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public class AddressContactValidator : AbstractValidator<AddressContact>
        {

            /// <summary>
            /// 
            /// </summary>
            public AddressContactValidator()
            {
                Property(x => x.Line1).NotNull();
                Property(x => x.Line2).NotNull();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public class PhoneContactValidator : AbstractValidator<PhoneContact>
        {

            /// <summary>
            /// 
            /// </summary>
            public PhoneContactValidator()
            {
                Property(x => x.Number).NotNull();
            }

        }

        #endregion

    }

}

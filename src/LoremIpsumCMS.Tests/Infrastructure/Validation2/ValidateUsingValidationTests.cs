﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using FluentRegistration;
using LoremIpsumCMS.Infrastructure.Validation2;
using LoremIpsumCMS.Tests.Infrastructure.Validation2.Classes;

using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Validation2
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidateUsingValidationTests
    {

        private readonly IServiceProvider _serviceProvider;

        /// <summary>
        /// 
        /// </summary>
        public ValidateUsingValidationTests()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.Register(Component
                .For<AddressContactValidator>()
                .ImplementedBy<AddressContactValidator>());
            _serviceProvider = serviceCollection.BuildServiceProvider();
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateUsing()
        {
            var tested = new CustomerMainAddressValidateUsingValidator();
            var customer = new Customer
            {
                Contacts = new AbstractContact[]
                {
                    new AddressContact()
                }
            };
            var context = new ValidationContext<Customer>(customer, _serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Equal(2, messages.Count);
            Assert.Contains(messages, x => x.PropertyName == "MainAddress.Line1");
            Assert.Contains(messages, x => x.Text == "Line1 must not be null");
            Assert.Contains(messages, x => x.PropertyName == "MainAddress.Line2");
            Assert.Contains(messages, x => x.Text == "Line2 must not be null");
        }

        #region Validators

        /// <summary>
        /// 
        /// </summary>
        public class CustomerMainAddressValidateUsingValidator : AbstractValidator<Customer>
        {

            /// <summary>
            /// 
            /// </summary>
            public CustomerMainAddressValidateUsingValidator()
            {
                Property(x => x.MainAddress).ValidateUsing<AddressContactValidator>();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public class AddressContactValidator : AbstractValidator<AddressContact>
        {

            /// <summary>
            /// 
            /// </summary>
            public AddressContactValidator()
            {
                Property(x => x.Line1).NotNull();
                Property(x => x.Line2).NotNull();
            }

        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Raven.Client;
using LoremIpsumCMS.Infrastructure.EventStore;
using LoremIpsumCMS.Tests.Infrastructure.EventStore.TestClasses;

using Moq;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.EventStore
{

    /// <summary>
    /// 
    /// </summary>
    public class EventStoreSaveTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanSaveTwoEvents()
        {
            var documentSessionMock = new Mock<IDocumentSession>();
            documentSessionMock.Setup(x => x.Store(It.IsAny<PersistedEvent>()));

            var tested = new RavenEventStore(documentSessionMock.Object);
            var events = new[] { new SomeEvent { Id = "one" }, new SomeEvent { Id = "one" } };
            tested.SaveEvents(events);

            documentSessionMock.Verify(x => x.Store(It.IsAny<PersistedEvent>()), Times.Exactly(2));
        }

    }

}

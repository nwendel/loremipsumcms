﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Model;

using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure
{

    /// <summary>
    /// 
    /// </summary>
    public class TypeIdentifierCacheTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRegister()
        {
            var tested = new TypeIdentifierCache();
            tested.Register(typeof(TypeIdentifierCacheTests));

            var identifier = tested.GetIdentifier(typeof(TypeIdentifierCacheTests));
            var type = tested.GetType(identifier);

            Assert.NotNull(identifier);
            Assert.Same(typeof(TypeIdentifierCacheTests), type);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnRegisterNullType()
        {
            var tested = new TypeIdentifierCache();
            Assert.Throws<ArgumentNullException>(() => tested.Register(null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnRegisterAbstractType()
        {
            var tested = new TypeIdentifierCache();
            Assert.Throws<ArgumentException>(() => tested.Register(typeof(AbstractEntity)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnRegisterOpenGenericType()
        {
            var tested = new TypeIdentifierCache();
            Assert.Throws<ArgumentException>(() => tested.Register(typeof(List<>)));
        }

    }

}

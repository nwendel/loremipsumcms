﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using LoremIpsumCMS.Tests.Infrastructure.Classes;

using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure
{

    /// <summary>
    /// 
    /// </summary>
    public class DisposableTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanDispose()
        {
            var tested = new TestDisposable();
            tested.Dispose();

            Assert.Equal(1, TestDisposable.ManagedDisposeCount);
            Assert.Equal(1, TestDisposable.UnmanagedDisposeCount);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanDisposeTwice()
        {
            var tested = new TestDisposable();
            tested.Dispose();
            tested.Dispose();

            Assert.Equal(1, TestDisposable.ManagedDisposeCount);
            Assert.Equal(1, TestDisposable.UnmanagedDisposeCount);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanDestructorDispose()
        {
            var tested = new TestDisposable();
            tested = null;

            GC.Collect();
            GC.WaitForPendingFinalizers();

            Assert.Equal(0, TestDisposable.ManagedDisposeCount);
            Assert.Equal(1, TestDisposable.UnmanagedDisposeCount);
        }

    }

}

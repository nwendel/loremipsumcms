﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using System.Reflection;
using LoremIpsumCMS.Infrastructure.Reflection;

using Xunit;
using Xunit.Runner.DotNet;

namespace LoremIpsumCMS.Tests.Infrastructure.Reflection
{

    /// <summary>
    /// 
    /// </summary>
    public class ReflectionUtilityTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanFindApplicationAssemblies()
        {
            var assemblies = ReflectionUtility.FindApplicationAssemblies().ToList();

            Assert.Contains(typeof(Program).GetTypeInfo().Assembly, assemblies);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanFindLoremIpsumAssemblies()
        {
            var assemblies = ReflectionUtility.FindLoremIpsumAssemblies();

            Assert.Contains(typeof(LoremIpsumException).GetTypeInfo().Assembly, assemblies);
            Assert.Contains(typeof(ReflectionUtilityTests).GetTypeInfo().Assembly, assemblies);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanFindApplicationAndLoremIpsumAssemblies()
        {
            var assemblies = ReflectionUtility.FindApplicationAndLoremIpsumAssemblies();
            var distinctAssemblyCount = assemblies.Distinct().Count();

            Assert.Equal(distinctAssemblyCount, assemblies.Count());
            Assert.Contains(typeof(Program).GetTypeInfo().Assembly, assemblies);
            Assert.Contains(typeof(LoremIpsumException).GetTypeInfo().Assembly, assemblies);
            Assert.Contains(typeof(ReflectionUtilityTests).GetTypeInfo().Assembly, assemblies);
        }

    }

}

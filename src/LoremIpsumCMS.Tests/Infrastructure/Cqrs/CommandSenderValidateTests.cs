﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Microsoft.Extensions.Logging;
using Raven.Client;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.EventStore;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Tests.Infrastructure.Cqrs.TestClasses;

using Moq;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Cqrs
{

    /// <summary>
    /// 
    /// </summary>
    public class CommandSenderValidateTests
    {

        private RavenCommandSender _tested;
        private Mock<ILogger<RavenCommandSender>> _loggerMock;
        private Mock<IServiceProvider> _serviceProviderMock;
        private Mock<IValidationService> _validationServiceMock;
        private Mock<IEventStore> _eventStoreMock;
        private Mock<IDocumentSession> _documentSession;

        /// <summary>
        /// 
        /// </summary>
        public CommandSenderValidateTests()
        {
            _loggerMock = new Mock<ILogger<RavenCommandSender>>(MockBehavior.Loose);
            _serviceProviderMock = new Mock<IServiceProvider>();
            _validationServiceMock = new Mock<IValidationService>();
            _eventStoreMock = new Mock<IEventStore>();
            _documentSession = new Mock<IDocumentSession>();
            _tested = new RavenCommandSender(_loggerMock.Object, _serviceProviderMock.Object, _validationServiceMock.Object, _eventStoreMock.Object, _documentSession.Object);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateCommand()
        {
            var command = new SomeCreateCommand();
            var validatorMock = new Mock<IValidateCommand<SomeCreateCommand>>();
            validatorMock.Setup(x => x.Validate(command));
            _validationServiceMock.Setup(x => x.Validate(command)).Returns(new ValidationMessage[0]);
            _serviceProviderMock.Setup(x => x.GetService(typeof(IValidateCommand<SomeCreateCommand>))).Returns(validatorMock.Object);
            _tested.Validate(command);

            Mock.VerifyAll(_serviceProviderMock, validatorMock);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnValidatorMessage()
        {
            var command = new SomeCreateCommand();
            _validationServiceMock.Setup(x => x.Validate(command)).Returns(new[] { new ValidationMessage("asdf", "Is invalid") });

            var ex = Assert.Throws<ValidationException>(() => _tested.Validate(command));
            Assert.Equal("Is invalid", ex.Message);
            Mock.VerifyAll(_validationServiceMock);
        }

    }

}

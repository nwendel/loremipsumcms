﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Microsoft.Extensions.Logging;
using LoremIpsumCMS.Infrastructure.Cqrs;

using Moq;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Cqrs
{

    /// <summary>
    /// 
    /// </summary>
    public class CommandSenderInvalidArgumentTests
    {

        private Mock<ILogger<RavenCommandSender>> _loggerMock = new Mock<ILogger<RavenCommandSender>>(MockBehavior.Loose);

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnSendNullCommand()
        {
            var tested = new RavenCommandSender(_loggerMock.Object, null, null, null, null);
            var ex = Assert.Throws<ArgumentNullException>(() => tested.Send((ICommand)null));
            Assert.Equal("command", ex.ParamName);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnValidateNullCommand()
        {
            var tested = new RavenCommandSender(_loggerMock.Object, null, null, null, null);
            var ex = Assert.Throws<ArgumentNullException>(() => tested.Validate((ICommand)null));
            Assert.Equal("command", ex.ParamName);
        }

    }

}

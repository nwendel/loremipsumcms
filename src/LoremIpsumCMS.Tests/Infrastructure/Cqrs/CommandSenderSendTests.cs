﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Microsoft.Extensions.Logging;
using Raven.Client;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.EventStore;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Tests.Infrastructure.Cqrs.TestClasses;

using Moq;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Cqrs
{

    /// <summary>
    /// 
    /// </summary>
    public class CommandSenderSendTests
    {

        private RavenCommandSender _tested;
        private Mock<ILogger<RavenCommandSender>> _loggerMock;
        private Mock<IServiceProvider> _serviceProviderMock;
        private Mock<IValidationService> _validationServiceMock;
        private Mock<IEventStore> _eventStoreMock;
        private Mock<IDocumentSession> _documentSessionMock;

        /// <summary>
        /// 
        /// </summary>
        public CommandSenderSendTests()
        {
            _loggerMock = new Mock<ILogger<RavenCommandSender>>(MockBehavior.Loose);
            _serviceProviderMock = new Mock<IServiceProvider>();
            _validationServiceMock = new Mock<IValidationService>();
            _eventStoreMock = new Mock<IEventStore>();
            _documentSessionMock = new Mock<IDocumentSession>();
            _tested = new RavenCommandSender(_loggerMock.Object, _serviceProviderMock.Object, _validationServiceMock.Object, _eventStoreMock.Object, _documentSessionMock.Object);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRaiseAndApplyCreateEvent()
        {
            object aggregate = null;
            var command = new SomeCreateCommand();
            var commandHandler = new SomeCommandHandler();
            _serviceProviderMock.Setup(x => x.GetService(typeof(IValidateCommand<SomeCreateCommand>))).Returns(null);
            _serviceProviderMock.Setup(x => x.GetService(typeof(IHandleCommand<SomeCreateCommand>))).Returns(new SomeCommandHandler());
            _documentSessionMock.Setup(x => x.Store(It.IsAny<SomeAggregate>())).Callback<object>(a => aggregate = a);
            _documentSessionMock.Setup(x => x.SaveChanges());

            _tested.Send(command);

            Assert.IsType<SomeAggregate>(aggregate);
            Assert.True(((SomeAggregate)aggregate).SomeEventApplied);
            Mock.VerifyAll(_serviceProviderMock);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRaiseAndApplyUpdateEvent()
        {
            var aggregate = new SomeAggregate { AggregateId = "SomeId/1" };
            var command = new SomeUpdateCommand { AggregateId = aggregate.Id };
            var commandHandler = new SomeCommandHandler();
            _serviceProviderMock.Setup(x => x.GetService(typeof(IValidateCommand<SomeUpdateCommand>))).Returns(null);
            _serviceProviderMock.Setup(x => x.GetService(typeof(IHandleCommand<SomeUpdateCommand>))).Returns(new SomeCommandHandler());
            _documentSessionMock.Setup(x => x.Load<SomeAggregate>(aggregate.Id)).Returns(aggregate);
            _documentSessionMock.Setup(x => x.SaveChanges());

            _tested.Send(command);

            Assert.True(aggregate.SomeEventApplied);
            Mock.VerifyAll(_serviceProviderMock);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRaiseAndApplyTwoUpdateEvents()
        {
            var aggregate = new SomeAggregate { AggregateId = "SomeId/1" };
            var command = new SomeUpdateCommand { AggregateId = aggregate.Id, EventCount = 2 };
            var commandHandler = new SomeCommandHandler();
            _serviceProviderMock.Setup(x => x.GetService(typeof(IValidateCommand<SomeUpdateCommand>))).Returns(null);
            _serviceProviderMock.Setup(x => x.GetService(typeof(IHandleCommand<SomeUpdateCommand>))).Returns(new SomeCommandHandler());
            _documentSessionMock.Setup(x => x.Load<SomeAggregate>(aggregate.Id)).Returns(aggregate);
            _documentSessionMock.Setup(x => x.SaveChanges());

            _tested.Send(command);

            Assert.Equal(2, aggregate.SomeEventAppliedCount);
            Mock.VerifyAll(_serviceProviderMock);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanSendTwoCommands()
        {
            var aggregate = new SomeAggregate { AggregateId = "SomeId/1" };
            var command = new SomeUpdateCommand { AggregateId = aggregate.Id };
            var commandHandler = new SomeCommandHandler();
            _serviceProviderMock.Setup(x => x.GetService(typeof(IValidateCommand<SomeUpdateCommand>))).Returns(null);
            _serviceProviderMock.Setup(x => x.GetService(typeof(IHandleCommand<SomeUpdateCommand>))).Returns(new SomeCommandHandler());
            _documentSessionMock.Setup(x => x.Load<SomeAggregate>(aggregate.Id)).Returns(aggregate);
            _documentSessionMock.Setup(x => x.SaveChanges());

            _tested.Send(command);
            _tested.Send(command);

            Assert.Equal(2, aggregate.SomeEventAppliedCount);
            Mock.VerifyAll(_serviceProviderMock);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnInvalidAggregateId()
        {
            SomeAggregate aggregate = null;
            var command = new SomeUpdateCommand { AggregateId = "Invalid" };
            var commandHandler = new SomeCommandHandler();
            _serviceProviderMock.Setup(x => x.GetService(typeof(IValidateCommand<SomeUpdateCommand>))).Returns(null);
            _serviceProviderMock.Setup(x => x.GetService(typeof(IHandleCommand<SomeUpdateCommand>))).Returns(new SomeCommandHandler());
            _documentSessionMock.Setup(x => x.Load<SomeAggregate>(command.AggregateId)).Returns(aggregate);
            _documentSessionMock.Setup(x => x.SaveChanges());

            Assert.Throws<CqrsException>(() => _tested.Send(command));
        }

    }

}

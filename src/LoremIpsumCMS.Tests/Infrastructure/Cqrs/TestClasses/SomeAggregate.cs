﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Model;

namespace LoremIpsumCMS.Tests.Infrastructure.Cqrs.TestClasses
{

    /// <summary>
    /// 
    /// </summary>
    public class SomeAggregate : AbstractAggregate,
        IApplyEvent<SomeEvent>
    {

        /// <summary>
        /// 
        /// </summary>
        public string AggregateId
        {
            get { return Id; }
            set { Id = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool SomeEventApplied { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public int SomeEventAppliedCount { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(SomeEvent @event)
        {
            SomeEventApplied = true;
            SomeEventAppliedCount += 1;
        }

    }

}

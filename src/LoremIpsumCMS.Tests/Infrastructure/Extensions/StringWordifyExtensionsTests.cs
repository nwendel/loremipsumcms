﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Extensions;

using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Extensions
{

    /// <summary>
    /// 
    /// </summary>
    public class StringWordifyExtensionsTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanWordifyTwoWordsPascalCasing()
        {
            var value = "ArticleList".Wordify();
            Assert.Equal("Article List", value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanWordifyFourWords()
        {
            var value = "OneTwoThreeFour".Wordify();
            Assert.Equal("One Two Three Four", value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanWordifyNull()
        {
            string tested = null;
            var value = tested.Wordify();
            Assert.Null(value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanWordifySingleLowercase()
        {
            var value = "a".Wordify();
            Assert.Equal("a", value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanWordifySingleUppercase()
        {
            var value = "A".Wordify();
            Assert.Equal("A", value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanWordifyAllUppercase()
        {
            var value = "ASDF".Wordify();
            Assert.Equal("ASDF", value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanWordifyWordUppercaseWord()
        {
            var value = "QwertyASDFQwerty".Wordify();
            Assert.Equal("Qwerty ASDF Qwerty", value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanWordifyWordUppercase()
        {
            var value = "QwertyASDF".Wordify();
            Assert.Equal("Qwerty ASDF", value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanWordifyUppercaseWord()
        {
            var value = "ASDFQwerty".Wordify();
            Assert.Equal("ASDF Qwerty", value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanWordifyLowercaseWordWord()
        {
            var value = "qwertyAsdf".Wordify();
            Assert.Equal("qwerty Asdf", value);
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using LoremIpsumCMS.Infrastructure.Extensions;
using LoremIpsumCMS.Tests.Infrastructure.Extensions.TestClasses;

using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Extensions
{

    /// <summary>
    /// 
    /// </summary>
    public class ObjectMapToExtensionTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanMapInstance()
        {
            Mapper.Initialize(x => x.CreateMap<Source, Destination>());
            var source = new Source { Value = "Asdf" };
            var destination = source.MapTo<Destination>();

            Assert.Equal("Asdf", destination.Value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanMapNullInstance()
        {
            Mapper.Initialize(x => x.CreateMap<Source, Destination>());
            Source source = null;
            var destination = source.MapTo<Destination>();

            Assert.Null(destination);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanMapArray()
        {
            Mapper.Initialize(x => x.CreateMap<Source, Destination>());
            var source = new[] { new Source { Value = "Asdf" } };
            var destination = source.MapTo<Destination>();

            Assert.Equal(1, destination.Length);
            Assert.Equal("Asdf", destination[0].Value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanMapNullArray()
        {
            Mapper.Initialize(x => x.CreateMap<Source, Destination>());
            Source[] source = null;
            var destination = source.MapTo<Destination>();

            Assert.Null(destination);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanMapEnumerable()
        {
            Mapper.Initialize(x => x.CreateMap<Source, Destination>());
            var source = new List<Source> { new Source { Value = "Asdf" } };
            var destination = source.MapTo<Destination>();

            Assert.Equal(1, destination.Count());
            Assert.Equal("Asdf", destination.ElementAt(0).Value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanMapNullEnumerable()
        {
            Mapper.Initialize(x => x.CreateMap<Source, Destination>());
            List<Source> source = null;
            var destination = source.MapTo<Destination>();

            Assert.Null(destination);
        }

    }

}

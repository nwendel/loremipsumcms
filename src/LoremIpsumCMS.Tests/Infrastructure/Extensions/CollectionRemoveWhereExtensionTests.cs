﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using LoremIpsumCMS.Infrastructure.Extensions;

using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Extensions
{

    /// <summary>
    /// 
    /// </summary>
    public class CollectionRemoveWhereExtensionTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRemoveWhere()
        {
            var tested = new List<string>() { "zxcv", "asdf", "qwerty" };

            tested.RemoveWhere(x => x == "asdf");

            Assert.Equal(2, tested.Count);
            Assert.DoesNotContain("asdf", tested);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnNullRemoveRange()
        {
            var tested = new List<string>() { "zxcv" };

            Assert.Throws<ArgumentNullException>(() => tested.RemoveWhere(null));
        }

    }

}

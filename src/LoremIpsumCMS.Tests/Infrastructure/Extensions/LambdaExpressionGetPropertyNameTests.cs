﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq.Expressions;
using LoremIpsumCMS.Infrastructure.Extensions;
using LoremIpsumCMS.Tests.Infrastructure.Extensions.TestClasses;

using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Extensions
{

    /// <summary>
    /// 
    /// </summary>
    public class LambdaExpressionGetPropertyNameTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetPropertyName()
        {
            Expression<Func<TestObject, string>> tested = x => x.Value;
            var propertyName = tested.GetPropertyName();

            Assert.Equal("Value", propertyName);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetPropertyNameConvert()
        {
            Expression<Func<TestObject, object>> tested = x => x.Value;
            var propertyName = tested.GetPropertyName();

            Assert.Equal("Value", propertyName);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnGetPropertyNameMethodCall()
        {
            Expression<Func<TestObject, int>> tested = x => x.AnotherValueMethod();

            Assert.Throws<ArgumentException>(() => tested.GetPropertyName());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnGetPropertyNameMethodCallConvert()
        {
            Expression<Func<TestObject, object>> tested = x => x.AnotherValueMethod();

            Assert.Throws<ArgumentException>(() => tested.GetPropertyName());
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using LoremIpsumCMS.Infrastructure.Extensions;

using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Extensions
{

    /// <summary>
    /// 
    /// </summary>
    public class StringTrimEndsWithExtensionsTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanTrimMatch()
        {
            var value = "AsdfQwerty".TrimEndsWith("Qwerty");
            Assert.Equal("Asdf", value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanTrimNoMatch()
        {
            var value = "Asdf".TrimEndsWith("Qwerty");
            Assert.Equal("Asdf", value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanTrimNull()
        {
            string tested = null;
            var value = tested.TrimEndsWith("Qwerty");
            Assert.Null(value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnNullEndsWith()
        {
            Assert.Throws<ArgumentNullException>(() => "Asdf".TrimEndsWith(null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnNullEndsWiths()
        {
            Assert.Throws<ArgumentNullException>(() => "Asdf".TrimEndsWith("Qwerty", ""));
        }

    }

}

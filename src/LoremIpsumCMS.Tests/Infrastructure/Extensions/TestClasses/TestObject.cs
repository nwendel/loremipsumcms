﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;

namespace LoremIpsumCMS.Tests.Infrastructure.Extensions.TestClasses
{

    /// <summary>
    /// 
    /// </summary>
    public class TestObject
    {

        /// <summary>
        /// 
        /// </summary>
        public string Value { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string ValueMethod()
        {
            return string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        public int AnotherValue { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int AnotherValueMethod()
        {
            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ThrowException()
        {
            throw new ArgumentException();
        }

        /// <summary>
        /// 
        /// </summary>
        public object ThrowExceptionProperty
        {
            get { throw new ArgumentException(); }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Property1 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Property2 { get; set; }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using LoremIpsumCMS.Infrastructure.Extensions;

using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Extensions
{

    /// <summary>
    /// 
    /// </summary>
    public class EnumerableOneExtensionTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOneTrue()
        {
            var tested = new[] { "asdf" };

            Assert.True(tested.One());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOneFalse()
        {
            var tested = new string[0];

            Assert.False(tested.One());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCanOneNull()
        {
            object[] tested = null;

            Assert.Throws<ArgumentNullException>(() =>tested.One());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOneFalseWithTwo()
        {
            var tested = new[] { "asdf", "asdf" };

            Assert.False(tested.One());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOneWithPredicateTrue()
        {
            var tested = new[] { "asdf", "qwerty" };

            Assert.True(tested.One(x => x == "asdf"));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOneWithPredicateFalse()
        {
            var tested = new[] { "asdf" };

            Assert.False(tested.One(x => x != "asdf"));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOneWithPredicateFalseWithTwo()
        {
            var tested = new[] { "asdf", "qwerty" };

            Assert.False(tested.One(x => true));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnOneWithPredicateNullSelf()
        {
            string[] tested = null;

            Assert.Throws<ArgumentNullException>(() => tested.One(x => x == "asdf"));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnOneWithPredicateNullPredicate()
        {
            var tested = new[] { "asdf", "qwerty" };

            Assert.Throws<ArgumentNullException>(() => tested.One(null));
        }

    }

}

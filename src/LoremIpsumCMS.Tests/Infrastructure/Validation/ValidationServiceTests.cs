﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using FluentRegistration;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Tests.Infrastructure.Validation.Classes;

using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidationServiceTests
    {

        private readonly IServiceProvider _serviceProvider;

        /// <summary>
        /// 
        /// </summary>
        public ValidationServiceTests()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.Register(AllClasses
                .FromTypes(new[] 
                {
                    typeof(CustomerNameLengthValidator),
                    typeof(CustomerOrdersValidator)
                })
                .WithService.AllInterfaces());
            _serviceProvider = serviceCollection.BuildServiceProvider();
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidate()
        {
            var tested = new ValidationService(_serviceProvider);
            var instance = new Customer
            {
                Name = "Qwerty",
                Orders = new [] { new Order { Amount = 1 } }
            };

            var messages = tested.Validate(instance).ToList();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateInvalid()
        {
            var tested = new ValidationService(_serviceProvider);
            var instance = new Customer
            {
                Name = "123",
                Orders = new[] { new Order { Amount = -1 } }
            };

            var messages = tested.Validate(instance).ToList();

            Assert.Equal(2, messages.Count);
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Tests.Infrastructure.Validation.Classes;

using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class ComparableValidationTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateGreaterThanOrEqualTo()
        {
            var tested = new CustomerNameAndAgeGreaterThanOrEqualToValidator();
            var customer = new Customer { Age = 18 };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateGreaterThanOrEqualToTooLow()
        {
            var tested = new CustomerNameAndAgeGreaterThanOrEqualToValidator();
            var customer = new Customer { Age = 17 };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Equal(1, messages.Count);
            Assert.All(messages, x =>
            {
                Assert.Equal("Age", x.PropertyName);
                Assert.Equal("Age must be 18 or greater", x.Text);
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateGreaterThan()
        {
            var tested = new CustomerNameAndAgeGreaterThanValidator();
            var customer = new Customer { Age = 19 };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateGreaterThanTooLow()
        {
            var tested = new CustomerNameAndAgeGreaterThanValidator();
            var customer = new Customer { Age = 18 };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Equal(1, messages.Count);
            Assert.All(messages, x =>
            {
                Assert.Equal("Age", x.PropertyName);
                Assert.Equal("Age must be greater than 18", x.Text);
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateLessThanOrEqualTo()
        {
            var tested = new CustomerNameAndAgeLessThanOrEqualToValidator();
            var customer = new Customer { Age = 18 };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateLessThanOrEqualToTooHigh()
        {
            var tested = new CustomerNameAndAgeLessThanOrEqualToValidator();
            var customer = new Customer { Age = 19 };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Equal(1, messages.Count);
            Assert.All(messages, x =>
            {
                Assert.Equal("Age", x.PropertyName);
                Assert.Equal("Age must be 18 or less", x.Text);
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateLessThan()
        {
            var tested = new CustomerNameAndAgeLessThanValidator();
            var customer = new Customer { Age = 17 };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateLessThanTooHigh()
        {
            var tested = new CustomerNameAndAgeLessThanValidator();
            var customer = new Customer { Age = 18 };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Equal(1, messages.Count);
            Assert.All(messages, x =>
            {
                Assert.Equal("Age", x.PropertyName);
                Assert.Equal("Age must be less than 18", x.Text);
            });
        }

    }

}

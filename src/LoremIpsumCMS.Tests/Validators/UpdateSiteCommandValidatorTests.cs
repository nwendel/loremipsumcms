﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Validators;

using Xunit;

namespace LoremIpsumCMS.Tests.Validators
{

    /// <summary>
    /// 
    /// </summary>
    public class UpdateSiteCommandValidatorTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateAllDefault()
        {
            var updateSiteCommand = new UpdateSiteCommand();
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<UpdateSiteCommand>(updateSiteCommand, serviceProvider);
            var tested = new UpdateSiteCommandValidator();
            var messages = tested.Validate(context).ToList();

            Assert.Equal(3, messages.Count);
            Assert.Contains(messages, x => x.PropertyName == "Name");
            Assert.Contains(messages, x => x.Text == "Name must not be null");
            Assert.Contains(messages, x => x.PropertyName == "ExtensionProperties");
            Assert.Contains(messages, x => x.Text == "ExtensionProperties must not be null");
            Assert.Contains(messages, x => x.PropertyName == "UpdatedAt");
            Assert.Contains(messages, x => x.Text == "UpdatedAt must be set");
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateNullExtensionProperty()
        {
            var updateSiteCommand = new UpdateSiteCommand
            {
                Name = "Some Name",
                ExtensionProperties = new AbstractSiteExtensionProperties[1],
                UpdatedAt = DateTime.Now
            };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<UpdateSiteCommand>(updateSiteCommand, serviceProvider);
            var tested = new UpdateSiteCommandValidator();
            var messages = tested.Validate(context).ToList();

            Assert.Equal(1, messages.Count);
            Assert.All(messages, x =>
            {
                Assert.Equal("ExtensionProperties[0]", x.PropertyName);
                Assert.Equal("Values for ExtensionProperties must not be null", x.Text);
            });
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Tests.Validators.Classes;
using LoremIpsumCMS.Validators;

using Xunit;

namespace LoremIpsumCMS.Tests.Validators
{

    /// <summary>
    /// 
    /// </summary>
    public class CreatePageCommandValidatorTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateAllDefault()
        {
            var createPageCommand = new CreatePageCommand();
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<CreatePageCommand>(createPageCommand, serviceProvider);
            var tested = new CreatePageCommandValidator();
            var messages = tested.Validate(context).ToList();

            Assert.Equal(6, messages.Count);
            Assert.Contains(messages, x => x.PropertyName == "SiteId");
            Assert.Contains(messages, x => x.Text == "SiteId must not be null");
            Assert.Contains(messages, x => x.PropertyName == "Path");
            Assert.Contains(messages, x => x.Text == "Path must not be null");
            Assert.Contains(messages, x => x.PropertyName == "Title");
            Assert.Contains(messages, x => x.Text == "Title must not be null");
            Assert.Contains(messages, x => x.PropertyName == "Data");
            Assert.Contains(messages, x => x.Text == "Data must not be null");
            Assert.Contains(messages, x => x.PropertyName == "ExtensionProperties");
            Assert.Contains(messages, x => x.Text == "ExtensionProperties must not be null");
            Assert.Contains(messages, x => x.PropertyName == "CreatedAt");
            Assert.Contains(messages, x => x.Text == "CreatedAt must be set");
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateNullExtensionProperty()
        {
            var createPageCommand = new CreatePageCommand
            {
                SiteId = "Site/1",
                Path = "/",
                Title = "Title",
                Data = new TestPageData(),
                ExtensionProperties = new AbstractPageExtensionProperties[1],
                CreatedAt = DateTime.Now
            };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<CreatePageCommand>(createPageCommand, serviceProvider);
            var tested = new CreatePageCommandValidator();
            var messages = tested.Validate(context).ToList();

            Assert.Equal(1, messages.Count);
            Assert.All(messages, x =>
            {
                Assert.Equal("ExtensionProperties[0]", x.PropertyName);
                Assert.Equal("Values for ExtensionProperties must not be null", x.Text);
            });
        }

    }

}

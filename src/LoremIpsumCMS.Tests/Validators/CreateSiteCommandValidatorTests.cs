﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Validators;

using Xunit;

namespace LoremIpsumCMS.Tests.Validators
{

    /// <summary>
    /// 
    /// </summary>
    public class CreateSiteCommandValidatorTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateAllDefault()
        {
            var createSiteCommand = new CreateSiteCommand();
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<CreateSiteCommand>(createSiteCommand, serviceProvider);
            var tested = new CreateSiteCommandValidator();
            var messages = tested.Validate(context).ToList();

            Assert.Equal(2, messages.Count);
            Assert.Contains(messages, x => x.PropertyName == "Name");
            Assert.Contains(messages, x => x.Text == "Name must not be null");
            Assert.Contains(messages, x => x.PropertyName == "CreatedAt");
            Assert.Contains(messages, x => x.Text == "CreatedAt must be set");
        }

    }

}

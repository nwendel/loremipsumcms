﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Module.Admin.Areas.SiteAdmin.Controllers;

using MyTested.AspNetCore.Mvc;
using Xunit;

namespace LoremIpsumCMS.Module.Admin.Tests.Areas.SiteAdmin.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class RouteTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRouteAdminSiteCreate()
        {
            MyRouting
                .Configuration()
                .ShouldMap(x => x
                    .WithMethod(HttpMethod.Get)
                    .WithLocation("/admin/site/create"))
                .To<SiteAdminController>(x => x.Create());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRouteAdminSiteEdit()
        {
            MyRouting
                .Configuration()
                .ShouldMap(x => x
                    .WithMethod(HttpMethod.Get)
                    .WithLocation("/admin/site/edit"))
                .To<SiteAdminController>(x => x.Edit(null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRouteAdminSiteEditSave()
        {
            MyRouting
                .Configuration()
                .ShouldMap(x => x
                    .WithMethod(HttpMethod.Post)
                    .WithLocation("/admin/site/edit/save"))
                .To<SiteAdminController>(x => x.EditSave(null));
        }

    }

}

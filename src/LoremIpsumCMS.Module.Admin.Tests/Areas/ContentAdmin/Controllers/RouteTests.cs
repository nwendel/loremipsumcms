﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Module.Admin.Areas.ContentAdmin.Controllers;

using MyTested.AspNetCore.Mvc;
using Xunit;

namespace LoremIpsumCMS.Module.Admin.Tests.Areas.ContentAdmin.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class RouteTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRouteAdminPageIndex()
        {
            MyRouting
                .Configuration()
                .ShouldMap(x => x
                    .WithMethod(HttpMethod.Get)
                    .WithLocation("/admin/pages"))
                .To<PageAdminController>(x => x.Index(null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRouteAdminPageCreate()
        {
            MyRouting
                .Configuration()
                .ShouldMap(x => x
                    .WithMethod(HttpMethod.Get)
                    .WithLocation("/admin/pages/create"))
                .To<PageAdminController>(x => x.Create());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRouteAdminPageCreateSave()
        {
            MyRouting
                .Configuration()
                .ShouldMap(x => x
                    .WithMethod(HttpMethod.Post)
                    .WithLocation("/admin/pages/create/save"))
                .To<PageAdminController>(x => x.CreateSave(null, null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRouteAdminPageEdit()
        {
            MyRouting
                .Configuration()
                .ShouldMap(x => x
                    .WithMethod(HttpMethod.Get)
                    .WithLocation("/admin/pages/edit"))
                .To<PageAdminController>(x => x.Edit(null, null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRouteAdminPageEditSave()
        {
            MyRouting
                .Configuration()
                .ShouldMap(x => x
                    .WithMethod(HttpMethod.Post)
                    .WithLocation("/admin/pages/edit/save"))
                .To<PageAdminController>(x => x.EditSave(null, null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRouteAdminPageDelete()
        {
            MyRouting
                .Configuration()
                .ShouldMap(x => x
                    .WithMethod(HttpMethod.Get)
                    .WithLocation("/admin/pages/delete"))
                .To<PageAdminController>(x => x.Delete(null, null));
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Raven.Client;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Module.Admin.Areas.ContentAdmin.Controllers;
using LoremIpsumCMS.Services;

using Moq;
using MyTested.AspNetCore.Mvc;
using LoremIpsumCMS.Module.Admin.Areas.ContentAdmin.ViewModels.PageAdmin;

namespace LoremIpsumCMS.Module.Admin.Tests.Areas.ContentAdmin.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class PageAdminControllerIndexTests
    {

        /// <summary>
        /// 
        /// </summary>
        //[Fact]
        public void CanIndex()
        {
            var siteReference = new SiteReference("Site/1", "Some Site");
            var pageOverviews = new PageOverview[0];

            var documentSessionMock = new Mock<IDocumentSession>();
            var contentTypeServiceMock = new Mock<IContentTypeService>();
            var commandSenderMock = new Mock<ICommandSender>();
            //var ravenQueryableMock = new Mock<IRavenQueryable<PageOverview>>();

            //documentSessionMock.Setup(x => x.Query<PageOverview>()).Returns(new Asdf<PageOverview>());
            //ravenQueryableMock.Setup(x => x.Customize(It.IsAny<Action<IDocumentQueryCustomization>>())).Returns(ravenQueryableMock.Object);
            //ravenQueryableMock.Setup(x => x.Where(It.IsAny<Expression<Func<PageOverview, bool>>>())).Returns(ravenQueryableMock.Object);
            //ravenQueryableMock.Setup(x => x.TakeAll()).Returns(pageOverviews.AsQueryable());

            MyController<PageAdminController>
                .Instance()
                .WithServices(x => x
                    .With(documentSessionMock.Object)
                    .With(contentTypeServiceMock.Object)
                    .With(commandSenderMock.Object))
                .Calling(x => x.Index(siteReference))
                .ShouldReturn()
                .View()
                .WithModel(new IndexViewModel
                {
                    Pages = new IndexPageViewModel[0]
                });
        }

    }

}

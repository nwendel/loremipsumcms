﻿using LoremIpsumCMS.Infrastructure.Raven;
using Raven.Client.UniqueConstraints;
using Sample.Web.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sample.Web.Queries
{

    /// <summary>
    /// 
    /// </summary>
    public static class MenuPageLoadByExtensions
    {

        #region By Page Id

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="pageId"></param>
        /// <returns></returns>
        public static MenuPage ByPageId(this RavenSessionLoadBy<MenuPage> self, string pageId)
        {
            if (string.IsNullOrEmpty(pageId))
            {
                throw new ArgumentNullException(nameof(pageId));
            }

            var menuPage = self.Session.LoadByUniqueConstraint<MenuPage>(x => x.PageId, pageId);
            return menuPage;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Raven.Client;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using Sample.Web.Model;
using Sample.Web.Queries;

namespace Sample.Web.EventHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class PageEventHandler :
        ISubscribeTo<PageCreatedEvent>,
        ISubscribeTo<PageUpdatedEvent>,
        ISubscribeTo<PageDeletedEvent>
    {

        #region Dependencies

        private readonly IDocumentSession _documentSession;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        public PageEventHandler(IDocumentSession documentSession)
        {
            _documentSession = documentSession;
        }

        #endregion

        #region Handle Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Handle(PageCreatedEvent @event)
        {
            var isIncluded = @event.GetExtensionProperty<MenuExtensionProperties>(x => x.IsIncluded);
            if(isIncluded)
            {
                var index = @event.GetExtensionProperty<MenuExtensionProperties>(x => x.Index);
                var menuPage = new MenuPage(
                    @event.Id,
                    @event.SiteId,
                    @event.Path,
                    index);
                _documentSession.Store(menuPage);
            }
        }

        #endregion

        #region Handle Updated

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Handle(PageUpdatedEvent @event)
        {
            var menuPage = _documentSession.LoadBy<MenuPage>(x => x.ByPageId(@event.Id));
            var isIncluded = @event.GetExtensionProperty<MenuExtensionProperties>(x => x.IsIncluded);
            
            if(menuPage != null && !isIncluded)
            {
                _documentSession.Delete(menuPage);
            }
            else if(isIncluded)
            {
                var index = @event.GetExtensionProperty<MenuExtensionProperties>(x => x.Index);
                if (menuPage == null)
                {
                    var page = _documentSession.Load<Page>(@event.Id);
                    menuPage = new MenuPage(
                        @event.Id,
                        page.SiteId,
                        @event.Path,
                        index);
                    _documentSession.Store(menuPage);
                }
                else
                {
                    menuPage.Update(@event.Path, index);
                }
            }
        }

        #endregion

        #region Handle Deleted

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Handle(PageDeletedEvent @event)
        {
            var menuPage = _documentSession.LoadBy<MenuPage>(x => x.ByPageId(@event.Id));
            if(menuPage != null)
            {
                _documentSession.Delete(menuPage);
            }
        }

        #endregion

    }

}

﻿using LoremIpsumCMS.Model;

namespace Sample.Web.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class ArticlePageData : AbstractPageData
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string Text { get; set; }

        #endregion

    }

}

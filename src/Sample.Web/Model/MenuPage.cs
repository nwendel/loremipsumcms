﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Raven.Client.UniqueConstraints;
using LoremIpsumCMS.Infrastructure.Model;

namespace Sample.Web.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class MenuPage : AbstractReadModel
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        protected MenuPage()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="siteId"></param>
        /// <param name="path"></param>
        /// <param name="index"></param>
        public MenuPage(string pageId, string siteId, string path, int index)
        {
            PageId = pageId;
            SiteId = siteId;
            Path = path;
            Index = index;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [UniqueConstraint]
        public string PageId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string SiteId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string Path { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public int Index { get; protected set; }

        #endregion

        #region Update

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="index"></param>
        public void Update(string path, int index)
        {
            Path = path;
            Index = index;
        }

        #endregion

    }

}

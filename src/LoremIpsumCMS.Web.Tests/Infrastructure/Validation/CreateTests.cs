﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Web.Infrastructure.Validation;

using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class CreateTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreate()
        {
            var message = new ValidationMessage(string.Empty, "Some message");
            var tested = new ValidationResult(new[] { message, message });

            Assert.True(tested.HasMessages);
            Assert.Equal(2, tested.Messages.Count());
            Assert.All(tested.Messages, x =>
            {
                Assert.Equal(string.Empty, x.PropertyName);
                Assert.Equal("Some message", x.Text);
            });
        }

    }

}

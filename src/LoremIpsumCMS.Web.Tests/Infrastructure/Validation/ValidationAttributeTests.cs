﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Mvc;
using LoremIpsumCMS.Web.Infrastructure.Validation;
using LoremIpsumCMS.Web.Tests.Infrastructure.Validation.TestClasses;

using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidationAttributeTests : AbstractValidationAttributeTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanReturnMessageFromValidateCallsInvalid()
        {
            var controller = new SomeController();
            var tested = new ValidateControllerActionAttribute();
            var context = CreateActionExecutingContext(controller, x => x.SomeActionWithValidationError());
            tested.OnActionExecuting(context);

            Assert.True(controller.ValidateCalled);
            Assert.True(controller.InvalidCalled);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanReturnMessageFromValidateCallsInvalidAsync()
        {
            var controller = new SomeController();
            var tested = new ValidateControllerActionAttribute();
            var context = CreateActionExecutingContext(controller, x => x.SomeActionWithValidationError());
            tested.OnActionExecutionAsync(context, () => { return null; });

            Assert.True(controller.ValidateCalled);
            Assert.True(controller.InvalidCalled);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanReturnSuccessFromValidateDoesNotCallInvalid()
        {
            var controller = new SomeController();
            var tested = new ValidateControllerActionAttribute();
            var context = CreateActionExecutingContext(controller, x => x.SomeActionWithSuccessValidation());
            tested.OnActionExecuting(context);

            Assert.True(controller.ValidateCalled);
            Assert.False(controller.InvalidCalled);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanExecuteFilterWithoutValidateMethod()
        {
            var controller = new SomeController();
            var tested = new ValidateControllerActionAttribute();
            var context = CreateActionExecutingContext(controller, x => x.SomeActionNoValidateMethod());
            tested.OnActionExecuting(context);

            Assert.False(controller.ValidateCalled);
            Assert.False(controller.InvalidCalled);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanExecuteFilterActionWithArguments()
        {
            var controller = new SomeController();
            var tested = new ValidateControllerActionAttribute();
            var context = CreateActionExecutingContext(controller, x => x.SomeActionWithArguments("asdf", new SomeClass()));
            tested.OnActionExecuting(context);

            Assert.True(controller.ValidateCalled);
            Assert.False(controller.InvalidCalled);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanExecuteFilterActionValidateRedirect()
        {
            var controller = new SomeController();
            var tested = new ValidateControllerActionAttribute();
            var context = CreateActionExecutingContext(controller, x => x.SomeActionValidateRedirect());
            tested.OnActionExecuting(context);

            Assert.True(context.Result is RedirectToRouteResult);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnExecuteFilterWithoutInvalidMethod()
        {
            var controller = new SomeController();
            var tested = new ValidateControllerActionAttribute();
            var context = CreateActionExecutingContext(controller, x => x.SomeActionNoInvalidMethod());

            Assert.Throws<ValidationMethodException>(() => tested.OnActionExecuting(context));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnExecuteFilterInvalidMethodReturnsNull()
        {
            var controller = new SomeController();
            var tested = new ValidateControllerActionAttribute();
            var context = CreateActionExecutingContext(controller, x => x.SomeActionInvalidReturnsNull());

            Assert.Throws<ValidationMethodException>(() => tested.OnActionExecuting(context));
        }

    }

}

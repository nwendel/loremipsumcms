﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using LoremIpsumCMS.Web.Infrastructure.Validation;
using LoremIpsumCMS.Web.Tests.Infrastructure.Validation.TestClasses;

using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidationResultStaticPropertyTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanUseStaticSuccessProperty()
        {
            var tested = ValidationResult.Success;

            Assert.True(tested.IsSuccess);
            Assert.False(tested.HasMessages);
            Assert.False(tested.IsRedirect);
            Assert.Empty(tested.Messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanUseStaticMessageProperty()
        {
            var tested = ValidationResult.Message("Some message {0}", "asdf");

            Assert.False(tested.IsSuccess);
            Assert.True(tested.HasMessages);
            Assert.False(tested.IsRedirect);
            Assert.Equal(1, tested.Messages.Count());
            Assert.All(tested.Messages, x =>
            {
                Assert.Equal(string.Empty, x.PropertyName);
                Assert.Equal("Some message asdf", x.Text);
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanUseStaticGenericMessageProperty()
        {
            var tested = ValidationResult.Message<SomeClass>(x => x.Asdf, "Some message {0}", "asdf");

            Assert.False(tested.IsSuccess);
            Assert.True(tested.HasMessages);
            Assert.False(tested.IsRedirect);
            Assert.Equal(1, tested.Messages.Count());
            Assert.All(tested.Messages, x =>
            {
                Assert.Equal("Asdf", x.PropertyName);
                Assert.Equal("Some message asdf", x.Text);
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanUseStaticGenericMessagePropertyStructProperty()
        {
            var tested = ValidationResult.Message<SomeClass>(x => x.Qwerty, "Some message {0}", "asdf");

            Assert.False(tested.IsSuccess);
            Assert.True(tested.HasMessages);
            Assert.False(tested.IsRedirect);
            Assert.Equal(1, tested.Messages.Count());
            Assert.All(tested.Messages, x =>
            {
                Assert.Equal("Qwerty", x.PropertyName);
                Assert.Equal("Some message asdf", x.Text);
            });
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using LoremIpsumCMS.Infrastructure.Validation;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractValidationAttributeTests
    {

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected ActionExecutingContext CreateActionExecutingContext<T>(T controller, Expression<Func<T, IActionResult>> actionExpression)
            where T : Controller
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<IValidationService, ValidationService>();
            var httpContext = new DefaultHttpContext();
            httpContext.RequestServices = serviceCollection.BuildServiceProvider();

            var methodCallExpression = (MethodCallExpression)actionExpression.Body;
            var methodInfo = methodCallExpression.Method;
            var routeData = new RouteData();
            var actionDescriptor = new ActionDescriptor();
            //actionDescriptor.DisplayName = methodInfo.Name;
            actionDescriptor.RouteValues["action"] = methodInfo.Name;
            actionDescriptor.Parameters = methodInfo.GetParameters()
                .Select(x => new ParameterDescriptor { Name = x.Name, ParameterType = x.ParameterType })
                .ToArray();
            var actionContext = new ActionContext(httpContext, routeData, actionDescriptor);
            var actionArguments = new Dictionary<string, object>();
            for(var ix = 0; ix < actionDescriptor.Parameters.Count; ix++)
            {
                var parameterName = actionDescriptor.Parameters[ix].Name;
                var argument = methodCallExpression.Arguments[ix];
                var lambdaExpression = Expression.Lambda(argument, actionExpression.Parameters);
                var argumentDelegate = lambdaExpression.Compile();
                var argumentValue = argumentDelegate.DynamicInvoke(new object[1]);
                actionArguments.Add(parameterName, argumentValue);
            }

            var context = new ActionExecutingContext(actionContext, new IFilterMetadata[0], actionArguments, controller);
            return context;
        }

    }

}

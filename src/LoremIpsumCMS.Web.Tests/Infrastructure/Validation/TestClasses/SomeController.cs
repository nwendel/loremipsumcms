﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Web.Infrastructure.Validation;
using Microsoft.AspNetCore.Mvc;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Validation.TestClasses
{

    /// <summary>
    /// 
    /// </summary>
    public class SomeController : Controller
    {

        /// <summary>
        /// 
        /// </summary>
        public bool ValidateCalled { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool InvalidCalled { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ValidationResult ValidateSomeActionWithValidationError()
        {
            ValidateCalled = true;
            return ValidationResult.Message<SomeClass>(x => x.Asdf, "Error");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult InvalidSomeActionWithValidationError()
        {
            InvalidCalled = true;
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult SomeActionWithValidationError()
        {
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ValidationResult ValidateSomeActionWithSuccessValidation()
        {
            ValidateCalled = true;
            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult InvalidSomeActionWithSuccessValidation()
        {
            InvalidCalled = true;
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult SomeActionWithSuccessValidation()
        {
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult SomeActionNoValidateMethod()
        {
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ValidationResult ValidateSomeActionNoInvalidMethod()
        {
            ValidateCalled = true;
            return ValidationResult.Message<SomeClass>(x => x.Asdf, "Error");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult SomeActionNoInvalidMethod()
        {
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ValidationResult ValidateSomeActionInvalidReturnsNull()
        {
            return ValidationResult.Message<SomeClass>(x => x.Asdf, "Error");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult InvalidSomeActionInvalidReturnsNull()
        {
            return null;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult SomeActionInvalidReturnsNull()
        {
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ValidationResult ValidateSomeActionWithArguments(string argument1, SomeClass argument2)
        {
            ValidateCalled = true;
            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult InvalidSomeActionWithArguments(string argument1, SomeClass argument2)
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult SomeActionWithArguments(string argument1, SomeClass argument2)
        {
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ValidationResult ValidateSomeActionValidateRedirect()
        {
            return ValidationResult.RedirectToAction("asdf");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult SomeActionValidateRedirect()
        {
            return null;
        }

    }

}

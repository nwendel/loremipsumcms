﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Reflection;
using LoremIpsumCMS.Web.Tests.Infrastructure.Module.TestClasses;

using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Module
{

    /// <summary>
    /// 
    /// </summary>
    public class ModuleTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetName()
        {
            var tested = new TestModule();
            Assert.Equal(typeof(TestModule).GetTypeInfo().Assembly.GetName().Name, tested.Name);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetAssembly()
        {
            var tested = new TestModule();
            Assert.Equal(typeof(TestModule).GetTypeInfo().Assembly, tested.Assembly);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetBaseNamespace()
        {
            var tested = new TestModule();
            Assert.Equal(typeof(TestModule).GetTypeInfo().Assembly.GetName().Name, tested.BaseNamespace);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanConfigure()
        {
            var tested = new TestModule();
            tested.Configure(null);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanConfigureServices()
        {
            var tested = new TestModule();
            tested.ConfigureServices(null);
        }

    }

}

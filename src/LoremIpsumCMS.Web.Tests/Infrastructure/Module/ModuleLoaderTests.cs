﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using AttachedProperties;
using LoremIpsumCMS.Web.Infrastructure.Module;
using LoremIpsumCMS.Web.Tests.Infrastructure.Module.TestClasses;

using Moq;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Module
{

    /// <summary>
    /// 
    /// </summary>
    public class ModuleLoaderTests
    {

        private Mock<ILogger<ModuleLoader>> _loggerMock = new Mock<ILogger<ModuleLoader>>(MockBehavior.Loose);

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadModule()
        {
            var tested = new ModuleLoader(_loggerMock.Object);

            var modules = tested.LoadAll();

            Assert.Equal(1, modules.Count());
            Assert.All(modules, x =>
            {
                Assert.Equal(typeof(TestModule), x);
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnGetModulesBeforeLoaded()
        {
            var tested = new ServiceCollection();

            Assert.Throws<LoremIpsumException>(() => tested.GetModules());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetModulesTwice()
        {
            var tested = new ServiceCollection();
            tested.AddSingleton<IModule, TestModule>();
            tested.SetAttachedValue(AttachedProperty.AreModulesLoaded, true);

            var first = tested.GetModules();
            var second = tested.GetModules();

            Assert.Same(first, second);
        }

    }

}

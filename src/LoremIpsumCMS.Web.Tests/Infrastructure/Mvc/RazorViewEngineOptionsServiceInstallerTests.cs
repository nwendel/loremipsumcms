﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using System.Reflection;
using AttachedProperties;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Options;
using LoremIpsumCMS.Web.Infrastructure.Module;
using LoremIpsumCMS.Web.Infrastructure.Mvc;
using LoremIpsumCMS.Web.Tests.Infrastructure.Module.TestClasses;

using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Mvc
{

    /// <summary>
    /// 
    /// </summary>
    public class RazorViewEngineOptionsServiceInstallerTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRegisterRazorViewEngineOptions()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.SetAttachedValue(AttachedProperty.AreModulesLoaded, true);
            serviceCollection.AddSingleton<IModule, TestModule>();
            var tested = new RazorViewEngineOptionsServiceInstaller();
            tested.Install(serviceCollection);
            var razorViewEngineOptions = new RazorViewEngineOptions();
            var optionsDescriptor = serviceCollection.Single(x => x.ServiceType == typeof(IConfigureOptions<RazorViewEngineOptions>));
            var instance = (ConfigureOptions<RazorViewEngineOptions>)optionsDescriptor.ImplementationInstance;
            instance.Action(razorViewEngineOptions);

            Assert.Equal(1, razorViewEngineOptions.FileProviders.Count);
            var fileProvider = razorViewEngineOptions.FileProviders[0];
            Assert.Equal(typeof(EmbeddedFileProvider), fileProvider.GetType());
            var embeddedFileProvider = (EmbeddedFileProvider)fileProvider;
            var assemblyFieldInfo = typeof(EmbeddedFileProvider).GetTypeInfo().GetField("_assembly", BindingFlags.NonPublic | BindingFlags.Instance);
            var assembly = assemblyFieldInfo.GetValue(embeddedFileProvider);
            Assert.Same(typeof(TestModule).GetTypeInfo().Assembly, assembly);
        }

    }

}

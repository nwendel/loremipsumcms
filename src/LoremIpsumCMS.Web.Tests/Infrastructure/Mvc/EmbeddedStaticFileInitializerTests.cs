﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.Extensions.DependencyInjection;
using LoremIpsumCMS.Web.Infrastructure.Module;
using LoremIpsumCMS.Web.Infrastructure.Mvc;
using LoremIpsumCMS.Web.Tests.Infrastructure.Module.TestClasses;
using LoremIpsumCMS.Web.Tests.Infrastructure.Mvc.Classes;

using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Mvc
{

    /// <summary>
    /// 
    /// </summary>
    public class EmbeddedStaticFileInitializerTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanInitialize()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<IModule, TestModule>();
            var serviceProvider = serviceCollection.BuildServiceProvider();

            var tested = new EmbeddedStaticFileInitializer(serviceProvider);
            var applicationBuilder = new FakeApplicationBuilder { ApplicationServices = serviceProvider };
            tested.Initialize(applicationBuilder);

            // TODO: I want to verify which static file providers have been added here, not sure how...
            // TODO: I need changes in the FakeApplicationBuilder too for this
            Assert.True(true);
        }

    }

}

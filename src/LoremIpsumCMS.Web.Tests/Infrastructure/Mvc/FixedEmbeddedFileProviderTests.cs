﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Reflection;
using LoremIpsumCMS.Web.Infrastructure.Mvc;

using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Mvc
{

    /// <summary>
    /// 
    /// </summary>
    public class FixedEmbeddedFileProviderTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetFileInfo()
        {
            var assembly = typeof(FixedEmbeddedFileProviderTests).GetTypeInfo().Assembly;
            var tested = new FixedEmbeddedFileProvider(assembly, assembly.GetName().Name);
            var fileInfo = tested.GetFileInfo("/Infrastructure/Mvc/EmbeddedFiles/One.txt");

            Assert.True(fileInfo.Exists);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetFileInfoWithDash()
        {
            var assembly = typeof(FixedEmbeddedFileProviderTests).GetTypeInfo().Assembly;
            var tested = new FixedEmbeddedFileProvider(assembly, assembly.GetName().Name);
            var fileInfo = tested.GetFileInfo("/Infrastructure/Mvc/EmbeddedFiles/With-Dash.txt");

            Assert.True(fileInfo.Exists);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetFileInfoFromDirectoryWithDash()
        {
            var assembly = typeof(FixedEmbeddedFileProviderTests).GetTypeInfo().Assembly;
            var tested = new FixedEmbeddedFileProvider(assembly, assembly.GetName().Name);
            var fileInfo = tested.GetFileInfo("/Infrastructure/Mvc/EmbeddedFiles/Folder-With-Dash/Two.txt");

            Assert.True(fileInfo.Exists);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnGetDirectoryContents()
        {
            var assembly = typeof(FixedEmbeddedFileProviderTests).GetTypeInfo().Assembly;
            var tested = new FixedEmbeddedFileProvider(assembly, assembly.GetName().Name);

            Assert.Throws<NotSupportedException>(() => tested.GetDirectoryContents("/"));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnWatch()
        {
            var assembly = typeof(FixedEmbeddedFileProviderTests).GetTypeInfo().Assembly;
            var tested = new FixedEmbeddedFileProvider(assembly, assembly.GetName().Name);

            Assert.Throws<NotSupportedException>(() => tested.Watch("/"));
        }

    }

}

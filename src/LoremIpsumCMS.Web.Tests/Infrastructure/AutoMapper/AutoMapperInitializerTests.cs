﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using AutoMapper;
using LoremIpsumCMS.Infrastructure.AutoMapper;
using LoremIpsumCMS.Infrastructure.Extensions;
using LoremIpsumCMS.Web.Tests.Infrastructure.AutoMapper.Classes;
using LoremIpsumCMS.Web.Tests.Infrastructure.Module.TestClasses;

using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.AutoMapper
{

    /// <summary>
    /// 
    /// </summary>
    public class AutoMapperInitializerTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanInitialize()
        {
            var modules = new[] { new TestModule() };
            var tested = new AutoMapperInitializer(modules);
            tested.Initialize();
            Mapper.AssertConfigurationIsValid();

            var source = new Source { Value = "Asdf" };
            var destination = source.MapTo<Destination>();
            Assert.Equal("Asdf", destination.Value);
        }

    }

}

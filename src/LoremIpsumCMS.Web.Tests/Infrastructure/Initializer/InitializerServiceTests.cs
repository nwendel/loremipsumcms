﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Builder.Internal;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using AttachedProperties;
using FluentRegistration;
using LoremIpsumCMS.Infrastructure.Initializer;
using LoremIpsumCMS.Web.Infrastructure.Initializer;
using LoremIpsumCMS.Web.Infrastructure.Module;
using LoremIpsumCMS.Web.Infrastructure.Raven;
using LoremIpsumCMS.Web.Tests.Infrastructure.Initializer.TestClasses;

using Moq;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Initializer
{

    /// <summary>
    /// 
    /// </summary>
    public class InitializerServiceTests
    {

        private Mock<ILogger<InitializerService>> _loggerMock = new Mock<ILogger<InitializerService>>(MockBehavior.Loose);

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanInstall()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.SetAttachedValue(AttachedProperty.AreModulesLoaded, true);
            serviceCollection.Install<InitializerServiceInstaller>();

            Assert.Contains(serviceCollection, x => x.ServiceType == typeof(IInitializerService));
            Assert.Contains(serviceCollection, x => x.ImplementationType == typeof(RavenWebInitializer));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanExecuteInitializers()
        {
            var someInitializer = new SomeInitializer();
            var anotherInitializer = new AnotherInitializer();
            var initializers = new IInitializer[] { someInitializer, anotherInitializer };

            var tested = new InitializerService(_loggerMock.Object, initializers, null);

            tested.ExecuteInitializers();

            Assert.True(someInitializer.Called);
            Assert.True(anotherInitializer.Called);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanExecuteApplicationBuilderInitializers()
        {
            var someInitializer = new SomeApplicationBuilderInitializer();
            var anotherInitializer = new AnotherApplicationBuilderInitializer();
            var initializers = new IApplicationBuilderInitializer[] { someInitializer, anotherInitializer };
            var tested = new InitializerService(_loggerMock.Object, null, initializers);
            var applicationBuilder = new ApplicationBuilder(new ServiceCollection().BuildServiceProvider());
            tested.ExecuteApplicationBuilderInitializers(applicationBuilder);

            Assert.True(someInitializer.Called);
            Assert.True(anotherInitializer.Called);
        }

    }

}

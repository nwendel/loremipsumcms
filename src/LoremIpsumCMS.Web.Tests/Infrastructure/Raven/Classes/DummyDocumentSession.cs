﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Linq;
using Raven.Client.Indexes;
using Raven.Abstractions.Data;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Raven.Classes
{

    /// <summary>
    /// 
    /// </summary>
    public class DummyDocumentSession : IDocumentSession
    {
        public ISyncAdvancedSessionOperation Advanced
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Delete(string id)
        {
            throw new NotImplementedException();
        }

        public void Delete<T>(ValueType id)
        {
            throw new NotImplementedException();
        }

        public void Delete<T>(T entity)
        {
            throw new NotImplementedException();
        }

        public bool IsDisposed { get; set; }

        public void Dispose()
        {
            IsDisposed = true;
        }

        public ILoaderWithInclude<object> Include(string path)
        {
            throw new NotImplementedException();
        }

        public ILoaderWithInclude<T> Include<T>(Expression<Func<T, object>> path)
        {
            throw new NotImplementedException();
        }

        public ILoaderWithInclude<T> Include<T, TInclude>(Expression<Func<T, object>> path)
        {
            throw new NotImplementedException();
        }

        public T[] Load<T>(params ValueType[] ids)
        {
            throw new NotImplementedException();
        }

        public T[] Load<T>(IEnumerable<ValueType> ids)
        {
            throw new NotImplementedException();
        }

        public T Load<T>(ValueType id)
        {
            throw new NotImplementedException();
        }

        public T[] Load<T>(IEnumerable<string> ids)
        {
            throw new NotImplementedException();
        }

        public T Load<T>(string id)
        {
            throw new NotImplementedException();
        }

        public TResult[] Load<TResult>(IEnumerable<string> ids, Type transformerType, Action<ILoadConfiguration> configure = null)
        {
            throw new NotImplementedException();
        }

        public TResult Load<TResult>(string id, Type transformerType, Action<ILoadConfiguration> configure = null)
        {
            throw new NotImplementedException();
        }

        public TResult[] Load<TResult>(IEnumerable<string> ids, string transformer, Action<ILoadConfiguration> configure = null)
        {
            throw new NotImplementedException();
        }

        public TResult Load<TResult>(string id, string transformer, Action<ILoadConfiguration> configure)
        {
            throw new NotImplementedException();
        }

        public TResult[] Load<TTransformer, TResult>(IEnumerable<string> ids, Action<ILoadConfiguration> configure = null) where TTransformer : AbstractTransformerCreationTask, new()
        {
            throw new NotImplementedException();
        }

        public TResult Load<TTransformer, TResult>(string id, Action<ILoadConfiguration> configure = null) where TTransformer : AbstractTransformerCreationTask, new()
        {
            throw new NotImplementedException();
        }

        public IRavenQueryable<T> Query<T>()
        {
            throw new NotImplementedException();
        }

        public IRavenQueryable<T> Query<T>(string indexName, bool isMapReduce = false)
        {
            throw new NotImplementedException();
        }

        public IRavenQueryable<T> Query<T, TIndexCreator>() where TIndexCreator : AbstractIndexCreationTask, new()
        {
            throw new NotImplementedException();
        }

        public void SaveChanges()
        {
            throw new NotImplementedException();
        }

        public void Store(object entity)
        {
            throw new NotImplementedException();
        }

        public void Store(object entity, string id)
        {
            throw new NotImplementedException();
        }

        public void Store(object entity, Etag etag)
        {
            throw new NotImplementedException();
        }

        public void Store(object entity, Etag etag, string id)
        {
            throw new NotImplementedException();
        }
    }

}


﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Raven.Abstractions.Data;
using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Indexes;
using Raven.Client.Linq;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Raven.Classes
{

    /// <summary>
    /// 
    /// </summary>
    public class DummyAsyncDocumentSession : IAsyncDocumentSession
    {
        public IAsyncAdvancedSessionOperations Advanced
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Delete(string id)
        {
            throw new NotImplementedException();
        }

        public void Delete<T>(ValueType id)
        {
            throw new NotImplementedException();
        }

        public void Delete<T>(T entity)
        {
            throw new NotImplementedException();
        }

        public bool IsDisposed { get; set; }

        public void Dispose()
        {
            IsDisposed = true;
        }

        public IAsyncLoaderWithInclude<object> Include(string path)
        {
            throw new NotImplementedException();
        }

        public IAsyncLoaderWithInclude<T> Include<T>(Expression<Func<T, object>> path)
        {
            throw new NotImplementedException();
        }

        public IAsyncLoaderWithInclude<T> Include<T, TInclude>(Expression<Func<T, object>> path)
        {
            throw new NotImplementedException();
        }

        public Task<T[]> LoadAsync<T>(CancellationToken token = default(CancellationToken), params ValueType[] ids)
        {
            throw new NotImplementedException();
        }

        public Task<T[]> LoadAsync<T>(IEnumerable<ValueType> ids, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<T> LoadAsync<T>(ValueType id, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<T[]> LoadAsync<T>(IEnumerable<string> ids, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<T> LoadAsync<T>(string id, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<TResult[]> LoadAsync<TResult>(IEnumerable<string> ids, Type transformerType, Action<ILoadConfiguration> configure = null, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<TResult> LoadAsync<TResult>(string id, Type transformerType, Action<ILoadConfiguration> configure = null, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<TResult[]> LoadAsync<TResult>(IEnumerable<string> ids, string transformer, Action<ILoadConfiguration> configure = null, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<TResult> LoadAsync<TResult>(string id, string transformer, Action<ILoadConfiguration> configure = null, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<TResult[]> LoadAsync<TTransformer, TResult>(IEnumerable<string> ids, Action<ILoadConfiguration> configure = null, CancellationToken token = default(CancellationToken)) where TTransformer : AbstractTransformerCreationTask, new()
        {
            throw new NotImplementedException();
        }

        public Task<TResult> LoadAsync<TTransformer, TResult>(string id, Action<ILoadConfiguration> configure = null, CancellationToken token = default(CancellationToken)) where TTransformer : AbstractTransformerCreationTask, new()
        {
            throw new NotImplementedException();
        }

        public IRavenQueryable<T> Query<T>()
        {
            throw new NotImplementedException();
        }

        public IRavenQueryable<T> Query<T>(string indexName, bool isMapReduce = false)
        {
            throw new NotImplementedException();
        }

        public IRavenQueryable<T> Query<T, TIndexCreator>() where TIndexCreator : AbstractIndexCreationTask, new()
        {
            throw new NotImplementedException();
        }

        public Task SaveChangesAsync(CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task StoreAsync(object entity, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task StoreAsync(object entity, string id, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task StoreAsync(object entity, Etag etag, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task StoreAsync(object entity, Etag etag, string id, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }
    }
}

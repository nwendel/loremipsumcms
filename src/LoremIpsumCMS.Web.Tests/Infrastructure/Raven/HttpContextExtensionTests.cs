﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Http;
using LoremIpsumCMS.Web.Infrastructure.Raven;
using LoremIpsumCMS.Web.Tests.Infrastructure.Raven.Classes;

using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public class HttpContextExtensionTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanSetAndGetDocumentSession()
        {
            var documentSession = new DummyDocumentSession();
            var tested = new DefaultHttpContext();

            tested.SetDocumentSession(documentSession);
            var value = tested.GetDocumentSession();

            Assert.Same(documentSession, value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanSetAndGetAsyncDocumentSession()
        {
            var asyncDocumentSession = new DummyAsyncDocumentSession();
            var tested = new DefaultHttpContext();

            tested.SetAsyncDocumentSession(asyncDocumentSession);
            var value = tested.GetAsyncDocumentSession();

            Assert.Same(asyncDocumentSession, value);
        }

    }

}

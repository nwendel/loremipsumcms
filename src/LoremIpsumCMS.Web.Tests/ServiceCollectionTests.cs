﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.Extensions.DependencyInjection;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Web.Infrastructure.Module;

using Xunit;

namespace LoremIpsumCMS.Web.Tests
{

    /// <summary>
    /// 
    /// </summary>
    public class ServiceCollectionTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanAddLoremIpsum()
        {
            var tested = new ServiceCollection();
            tested.AddLogging();
            tested.AddMvc();
            tested.AddLoremIpsum();

            Assert.Contains(tested, x => x.ServiceType == typeof(IValidationService));
            Assert.Contains(tested, x => x.ServiceType == typeof(IModuleLoader));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThowsOnAddLoremIpsumBeforeAddMvc()
        {
            var tested = new ServiceCollection();

            Assert.Throws<LoremIpsumException>(() => tested.AddLoremIpsum());
        }

    }

}

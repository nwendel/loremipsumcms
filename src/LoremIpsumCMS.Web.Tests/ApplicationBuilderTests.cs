﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Builder.Internal;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using LoremIpsumCMS.Infrastructure.Initializer;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Web.Infrastructure.Initializer;
using LoremIpsumCMS.Web.Infrastructure.Raven;
using LoremIpsumCMS.Web.Tests.Infrastructure.Initializer.TestClasses;

using Moq;
using Xunit;

namespace LoremIpsumCMS.Web.Tests
{

    /// <summary>
    /// 
    /// </summary>
    public class ApplicationBuilderTests
    {

        private Mock<ILogger<InitializerService>> _loggerMock = new Mock<ILogger<InitializerService>>(MockBehavior.Loose);

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanUseLoremIpsum()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<IInitializerService, InitializerService>();
            serviceCollection.AddSingleton(_loggerMock.Object);
            serviceCollection.AddSingleton<IInitializer, SomeInitializer>();
            serviceCollection.AddSingleton<IApplicationBuilderInitializer, SomeApplicationBuilderInitializer>();
            serviceCollection.AddSingleton<IDocumentSessionHolder, DocumentSessionHolder>();
            serviceCollection.AddSingleton(new DocumentStoreServiceFactory().Create());
            var serviceProvider = serviceCollection.BuildServiceProvider();
            var tested = new ApplicationBuilder(serviceProvider);

            tested.UseLoremIpsum();

            var someInitializer = (SomeInitializer)serviceProvider.GetService<IInitializer>();
            var someApplicationBuilderInitializer = (SomeApplicationBuilderInitializer)serviceProvider.GetService<IApplicationBuilderInitializer>();
            Assert.True(someInitializer.Called);
            Assert.True(someApplicationBuilderInitializer.Called);
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Services;
using LoremIpsumCMS.Web.Mvc.ModelBinding;
using LoremIpsumCMS.Web.Tests.Mvc.ModelBinding.Classes;

using Xunit;

namespace LoremIpsumCMS.Web.Tests.Mvc.ModelBinding
{

    /// <summary>
    /// 
    /// </summary>
    public class TypeIdentifierCacheInitializerTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanInitialize()
        {
            var contentTypeService = new ContentTypeService();
            var typeIdentifierCache = new TypeIdentifierCache();
            var tested = new TypeIdentifierCacheInitializer(contentTypeService, typeIdentifierCache);

            tested.Initialize();
            var identifier = typeIdentifierCache.GetIdentifier(typeof(SomeSiteExtensionProperties));

            Assert.NotNull(identifier);
        }

    }

}

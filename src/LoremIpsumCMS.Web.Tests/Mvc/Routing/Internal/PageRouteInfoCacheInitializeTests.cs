﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Microsoft.AspNetCore.Mvc;
using LoremIpsumCMS.Infrastructure.Extensions;
using LoremIpsumCMS.TestHelpers;
using LoremIpsumCMS.Web.Mvc.Routing.Internal;
using LoremIpsumCMS.Web.Tests.Mvc.Routing.Internal.Classes;

using Xunit;

namespace LoremIpsumCMS.Web.Tests.Mvc.Routing.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public class PageRouteInfoCacheInitializeTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanInitialize()
        {
            var tested = new PageRouteInfoCache(new NullLogger<PageRouteInfoCache>(), new IRouteParameterFactory[0]);
            tested.Initialize();

            var pageRouteInfo = tested.GetRouteInfo(typeof(SomePageData));

            Assert.Equal(nameof(SomeController).TrimEndsWith(nameof(Controller)), pageRouteInfo.ControllerName);
            Assert.Equal(nameof(SomeController.Some), pageRouteInfo.ActionName);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanInitializeNoReturnType()
        {
            var tested = new PageRouteInfoCache(new NullLogger<PageRouteInfoCache>(), new IRouteParameterFactory[0]);
            tested.Initialize();

            Assert.Throws<ArgumentException>(() => tested.GetRouteInfo(typeof(NoReturnTypePageData)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanInitializeInvalidReturnType()
        {
            var tested = new PageRouteInfoCache(new NullLogger<PageRouteInfoCache>(), new IRouteParameterFactory[0]);
            tested.Initialize();

            Assert.Throws<ArgumentException>(() => tested.GetRouteInfo(typeof(InvalidReturnTypePageData)));
        }

    }

}

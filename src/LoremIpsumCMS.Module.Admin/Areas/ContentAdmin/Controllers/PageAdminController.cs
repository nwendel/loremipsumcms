﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Raven.Client;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Extensions;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Module.Admin.Areas.ContentAdmin.ViewModels.PageAdmin;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Services;
using LoremIpsumCMS.Web.Infrastructure.Validation;
using LoremIpsumCMS.Web.Mvc.Filters;

namespace LoremIpsumCMS.Module.Admin.Areas.ContentAdmin.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [SiteAware]
    [Area("ContentAdmin")]
    [Route("admin/pages")]
    public class PageAdminController : Controller
    {

        #region Dependencies

        private readonly IDocumentSession _documentSession;
        private readonly IContentTypeService _contentTypeService;
        private readonly ICommandSender _commandSender;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        /// <param name="contentTypeService"></param>
        /// <param name="commandSender"></param>
        public PageAdminController(
            IDocumentSession documentSession,
            IContentTypeService contentTypeService,
            ICommandSender commandSender)
        {
            _documentSession = documentSession;
            _contentTypeService = contentTypeService;
            _commandSender = commandSender;
        }

        #endregion

        #region Index

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <returns></returns>
        public ValidationResult ValidateIndex(SiteReference siteReference)
        {
            if (siteReference == null)
            {
                return ValidationResult.NotFound;
            }
            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <returns></returns>
        [HttpGet("")]
        public ActionResult Index(SiteReference siteReference)
        {
            var pages = _documentSession.Query<PageOverview>()
                .Customize(o => o.WaitForNonStaleResultsAsOfNow())
                .Where(x => x.SiteId == siteReference.SiteId)
                .TakeAll()
                .ToList();

            var viewModel = new IndexViewModel
            {
                Pages = pages.MapTo<IndexPageViewModel>()
            };

            return View(viewModel);
        }

        #endregion

        #region Create

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("create")]
        public ActionResult Create()
        {
            // TODO: Page Type Name must be sent as a parameter...
            var pageTypes = _contentTypeService.FindPages();
            var pageData = (AbstractPageData)Activator.CreateInstance(pageTypes.First());
            var extensionPropertyTypes = _contentTypeService.FindPageExtensionProperties();
            var extensionProperties = extensionPropertyTypes
                .Select(x => (AbstractPageExtensionProperties)Activator.CreateInstance(x))
                .ToArray();

            var viewModel = new CreateViewModel
            {
                Data = pageData,
                ExtensionProperties = extensionProperties
            };

            return View(viewModel);
        }

        #endregion

        #region Create Save

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ValidationResult ValidateCreateSave(SiteReference siteReference, CreateViewModel viewModel)
        {
            if(siteReference == null)
            {
                return ValidationResult.NotFound;
            }
            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ActionResult InvalidCreateSave(SiteReference siteReference, CreateViewModel viewModel)
        {
            return View("Create", viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost("create/save")]
        public ActionResult CreateSave(SiteReference siteReference, CreateViewModel viewModel)
        {
            var command = new CreatePageCommand
            {
                SiteId = siteReference.SiteId,
                Path = viewModel.Path,
                Title = viewModel.Title,
                MetaKeywords = viewModel.MetaKeywords,
                MetaDescription = viewModel.MetaDescription,
                Data = viewModel.Data,
                ExtensionProperties = viewModel.ExtensionProperties,
                CreatedAt = DateTime.Now
            };
            _commandSender.Send(command);

            return RedirectToAction("Index");
        }

        #endregion

        #region Edit

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public ValidationResult ValidateEdit(SiteReference siteReference, string path)
        {
            if(siteReference == null)
            {
                return ValidationResult.NotFound;
            }
            var exists = _documentSession.ExistsBy<PageOverview>(x => x.BySiteIdAndPath(siteReference.SiteId, path));
            if(!exists)
            {
                // TODO: Or should I redirect to a page not found?
                return ValidationResult.NotFound;
            }
            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        [HttpGet("edit")]
        public ActionResult Edit(SiteReference siteReference, string path)
        {
            var page = _documentSession.LoadBy<Page>(x => x.BySiteIdAndPath(siteReference.SiteId, path));
            var extensionPropertyTypes = _contentTypeService.FindPageExtensionProperties();
            var addExtensionPropertyTypes = extensionPropertyTypes
                .Where(x => !x.In(page.ExtensionProperties.Select(p => p.GetType())))
                .ToList();
            var addExtensionProperties = addExtensionPropertyTypes
                .Select(x => (AbstractPageExtensionProperties)Activator.CreateInstance(x))
                .ToArray();
            var extensionProperties = page.ExtensionProperties
                .Concat(addExtensionProperties)
                .ToArray();

            var viewModel = new EditViewModel
            {
                OldPath = page.Path,
                Path = page.Path,
                Title = page.Title,
                MetaKeywords = page.MetaKeywords,
                MetaDescription = page.MetaDescription,
                Data = page.Data,
                ExtensionProperties = extensionProperties
            };
            return View(viewModel);
        }

        #endregion

        #region Edit Save

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ValidationResult ValidateEditSave(SiteReference siteReference, CreateViewModel viewModel)
        {
            if (siteReference == null)
            {
                return ValidationResult.NotFound;
            }
            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ActionResult InvalidEditSave(SiteReference siteReference, EditViewModel viewModel)
        {
            return View("Edit", viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost("edit/save")]
        public ActionResult EditSave(SiteReference siteReference, EditViewModel viewModel)
        {
            var page = _documentSession.LoadBy<Page>(x => x.BySiteIdAndPath(siteReference.SiteId, viewModel.OldPath));

            var command = new UpdatePageCommand
            {
                Id = page.Id,
                Path = viewModel.Path,
                Title = viewModel.Title,
                MetaKeywords = viewModel.MetaKeywords,
                MetaDescription = viewModel.MetaDescription,
                Data = viewModel.Data,
                ExtensionProperties = viewModel.ExtensionProperties,
                UpdatedAt = DateTime.Now
            };
            _commandSender.Send(command);
            
            return RedirectToAction("Index");
        }

        #endregion

        #region Delete

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public ValidationResult ValidateDelete(SiteReference siteReference, string path)
        {
            return ValidateEdit(siteReference, path);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        [HttpGet("delete")]
        public ActionResult Delete(SiteReference siteReference, string path)
        {
            var page = _documentSession.LoadBy<Page>(x => x.BySiteIdAndPath(siteReference.SiteId, path));
            var command = new DeletePageCommand
            {
                Id = page.Id
            };
            _commandSender.Send(command);

            return RedirectToAction("Index");
        }

        #endregion

    }

}

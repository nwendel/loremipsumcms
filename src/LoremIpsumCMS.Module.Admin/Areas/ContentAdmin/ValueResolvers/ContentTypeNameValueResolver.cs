﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using System.Reflection;
using AutoMapper;
using LoremIpsumCMS.Infrastructure.Reflection;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Module.Admin.Areas.ContentAdmin.ViewModels.PageAdmin;
using LoremIpsumCMS.Infrastructure.Extensions;

namespace LoremIpsumCMS.Module.Admin.Areas.ContentAdmin.ValueResolvers
{

    /// <summary>
    /// 
    /// </summary>
    public class ContentTypeNameValueResolver : IValueResolver<PageOverview, IndexPageViewModel, string>
    {

        #region Resolve

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <param name="destMember"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public string Resolve(PageOverview source, IndexPageViewModel destination, string destMember, ResolutionContext context)
        {
            var pageDataTypeFullName = source.DataTypeFullName;

            var name = pageDataTypeFullName
                .Substring(pageDataTypeFullName.LastIndexOf('.') + 1)
                .TrimEndsWith("Page", "Data", "PageData").Wordify();
            return name;
        }

        #endregion

    }

}

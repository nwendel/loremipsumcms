﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using LoremIpsumCMS.Module.Admin.Areas.AdminShell.ViewModels.ViewComponents;
using LoremIpsumCMS.Web.Mvc;

namespace LoremIpsumCMS.Module.Admin.Areas.AdminShell.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [Area("AdminShell")]
    public class ShellController : Controller
    {

        #region Dependencies

        private readonly IHttpContextAccessor _httpContextAccessor;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContextAccessor"></param>
        public ShellController(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Main Menu

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [ViewComponentAction]
        public ActionResult MainMenu()
        {
            var httpContext = _httpContextAccessor.HttpContext;
            var path = httpContext.Request.Path.ToString();
            var mainMenu = CreateMainMenuModel();
            UpdateIsActive(mainMenu, path);

            var viewModel = new AdminMenuSidebarViewModel
            {
                MainMenu = mainMenu
            };

            return PartialView("_MainMenu", viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private MenuItemViewModel[] CreateMainMenuModel()
        {
            var result = new[]
            {
                new MenuItemViewModel
                {
                    IconClass = "ti-home",
                    Title = "Dashboard",
                    Url = Url.Action("Index", "Dashboard", new { Area = "AdminShell" })
                },
                new MenuItemViewModel
                {
                    IconClass = "ti-layout",
                    Title = "Site",
                    Url = Url.Action("Edit", "SiteAdmin", new { Area = "SiteAdmin" })
                },
                new MenuItemViewModel
                {
                    IconClass = "ti-layers-alt",
                    Title = "Pages",
                    Url = Url.Action("Index", "PageAdmin", new { Area = "ContentAdmin" })
                },
                new MenuItemViewModel
                {
                    IconClass = "ti-layers-alt",
                    Title = "Partials",
                    Url = "#"
                },
                new MenuItemViewModel
                {
                    IconClass = "ti-files",
                    Title = "Assets",
                    Url = "#"
                },
                new MenuItemViewModel
                {
                    IconClass = "ti-info",
                    Title = "Documentation",
                    Children = new []
                    {
                        new MenuItemViewModel
                        {
                            Title = "Developer",
                            Url = "#"
                        },
                        new MenuItemViewModel
                        {
                            Title = "About",
                            Url = "#"
                        },
                    }
                }
            };
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="menuItems"></param>
        private void UpdateIsActive(MenuItemViewModel[] menuItems, string path)
        {
            if (menuItems == null)
            {
                return;
            }

            foreach (var menuItem in menuItems.OrderByDescending(x => (x.Url ?? "").Length))
            {
                UpdateIsActive(menuItem.Children, path);
                if (path.StartsWith(menuItem.Url ?? "#"))
                {
                    menuItem.IsActive = true;
                    break;
                }

            }
        }

        #endregion

    }

}

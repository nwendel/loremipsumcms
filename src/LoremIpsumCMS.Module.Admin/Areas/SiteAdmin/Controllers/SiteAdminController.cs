﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Extensions;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Module.Admin.Areas.SiteAdmin.ViewModels.SiteAdmin;
using LoremIpsumCMS.Services;
using LoremIpsumCMS.Web.Infrastructure.Extensions;
using LoremIpsumCMS.Web.Mvc.Filters;
using LoremIpsumCMS.Web.Infrastructure.Validation;
using Raven.Client;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Queries;

namespace LoremIpsumCMS.Module.Admin.Areas.SiteAdmin.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [SiteAware]
    [Area("SiteAdmin")]
    [Route("admin/site")]
    public class SiteAdminController : Controller
    {

        #region Dependencies

        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IContentTypeService _contentTypeService;
        private readonly ICommandSender _commandSender;
        private readonly IDocumentSession _documentSession;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContextAccessor"></param>
        /// <param name="contentTypeService"></param>
        /// <param name="commandSender"></param>
        /// <param name="documentSession"></param>
        public SiteAdminController(
            IHttpContextAccessor httpContextAccessor,
            IContentTypeService contentTypeService,
            ICommandSender commandSender,
            IDocumentSession documentSession)
        {
            _httpContextAccessor = httpContextAccessor;
            _contentTypeService = contentTypeService;
            _commandSender = commandSender;
            _documentSession = documentSession;
        }

        #endregion

        #region Create

        /// <summary>
        /// 
        /// </summary>
        [HttpGet("create")]
        public ActionResult Create()
        {
            var host = _httpContextAccessor.GetRequestHost().Host;
            var extensionPropertyTypes = _contentTypeService.FindSiteExtensionProperties();
            var extensionProperties = extensionPropertyTypes
                .Select(x => (AbstractSiteExtensionProperties)Activator.CreateInstance(x))
                .ToArray();

            var viewModel = new CreateViewModel
            {
                Hosts = new[] 
                {
                    new SiteHostViewModel
                    {
                        Name = host,
                        IsAdmin = true
                    }
                },
                ExtensionProperties = extensionProperties
            };

            return View(viewModel);
        }

        #endregion

        #region Create Save

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ActionResult InvalidCreateSave(CreateViewModel viewModel)
        {
            return View("Create", viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost("create/save")]
        public ActionResult CreateSave(CreateViewModel viewModel)
        {
            var hosts = viewModel.Hosts
                .Where(x => !x.IsDeleted)
                .MapTo<SiteHost>()
                .ToList();

            var command = new CreateSiteCommand
            {
                Name = viewModel.Name,
                Hosts = hosts,
                ExtensionProperties = viewModel.ExtensionProperties,
                CreatedAt = DateTime.Now
            };
            _commandSender.Send(command);

            return RedirectToAction("Index", "Dashboard", new { Area = "AdminShell" });
        }

        #endregion

        #region Edit

        /// <summary>
        /// 
        /// </summary>
        /// <param name="site"></param>
        /// <returns></returns>
        public ValidationResult ValidateEdit(Site site)
        {
            if(site == null)
            {
                return ValidationResult.NotFound;
            }
            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="site"></param>
        /// <returns></returns>
        [HttpGet("edit")]
        public ActionResult Edit(Site site)
        {
            var extensionPropertyTypes = _contentTypeService.FindSiteExtensionProperties();
            var addExtensionPropertyTypes = extensionPropertyTypes
                .Where(x => !x.In(site.ExtensionProperties.Select(p => p.GetType())))
                .ToList();
            var addExtensionProperties = addExtensionPropertyTypes
                .Select(x => (AbstractSiteExtensionProperties)Activator.CreateInstance(x))
                .ToArray();
            var extensionProperties = site.ExtensionProperties
                .Concat(addExtensionProperties)
                .ToArray();

            var viewModel = new EditViewModel
            {
                Name = site.Name,
                OldNameSlug = site.NameSlug,
                Hosts = site.Hosts.MapTo<SiteHostViewModel>().ToArray(),
                ExtensionProperties = extensionProperties
            };
            return View(viewModel);
        }

        #endregion

        #region Edit Save

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ActionResult InvalidEditSave(EditViewModel viewModel)
        {
            return View("Edit", viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost("edit/save")]
        public ActionResult EditSave(EditViewModel viewModel)
        {
            var site = _documentSession.LoadBy<Site>(x => x.ByNameSlug(viewModel.OldNameSlug));

            var hosts = viewModel.Hosts
                .Where(x => !x.IsDeleted)
                .MapTo<SiteHost>()
                .ToList();
            var command = new UpdateSiteCommand
            {
                Id = site.Id,
                Name = viewModel.Name,
                Hosts = hosts,
                ExtensionProperties = viewModel.ExtensionProperties,
                UpdatedAt = DateTime.Now
            };
            _commandSender.Send(command);

            return RedirectToAction("Index", "Dashboard", new { Area = "AdminShell" });
        }

        #endregion

    }

}

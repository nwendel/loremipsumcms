﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Raven.Client;
using LoremIpsumCMS.Infrastructure.Extensions;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Module.Admin.Areas.SiteAdmin.ViewModels.SiteAdmin;
using LoremIpsumCMS.Queries;

namespace LoremIpsumCMS.Module.Admin.Areas.SiteAdmin.Validators.SiteAdmin
{

    /// <summary>
    /// 
    /// </summary>
    public class EditViewModelValidator : AbstractCreateOrEditViewModelValidator<EditViewModel>
    {

        #region Dependencies

        private readonly IDocumentSession _documentSession;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        public EditViewModelValidator(
            IHttpContextAccessor httpContextAccessor,
            IDocumentSession documentSession)
            : base(httpContextAccessor)
        {
            _documentSession = documentSession;

            ValidateUsing(ValidateNameUnique);
            ValidateUsing(ValidateHostsUnique);
        }

        #endregion

        #region Validate Name Unique

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private IEnumerable<ValidationMessage> ValidateNameUnique(ValidationContext<EditViewModel> context)
        {
            var siteName = context.Instance.Name;
            if (string.IsNullOrWhiteSpace(siteName))
            {
                yield break;
            }

            var siteNameSlug = siteName.Slugify();
            var exists = _documentSession.ExistsBy<Model.Site>(x => x.ByNameSlug(siteNameSlug));
            if (exists)
            {
                var oldSite = _documentSession.LoadBy<Model.Site>(x => x.ByNameSlug(context.Instance.OldNameSlug));
                var existingSite = _documentSession.LoadBy<Model.Site>(x => x.ByNameSlug(siteNameSlug));
                if (oldSite.Id != existingSite.Id)
                {
                    yield return new ValidationMessage("Name", "Name must be unique");
                }
            }
        }

        #endregion
        
        #region Validate Hosts Unique

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private IEnumerable<ValidationMessage> ValidateHostsUnique(ValidationContext<EditViewModel> context)
        {
            var site = _documentSession.LoadBy<Site>(x => x.ByNameSlug(context.Instance.OldNameSlug));
            var hosts = context.Instance.Hosts;
            foreach(var host in hosts)
            {
                    var otherHost = _documentSession.LoadBy<Host>(x => x.ByName(host.Name));
                    if(otherHost != null && otherHost.SiteId != site.Id)
                    {
                            yield return new ValidationMessage("Hosts", "Hostnames must be unique");
                    }
            }

            yield break;
        }

        #endregion
        

    }

}

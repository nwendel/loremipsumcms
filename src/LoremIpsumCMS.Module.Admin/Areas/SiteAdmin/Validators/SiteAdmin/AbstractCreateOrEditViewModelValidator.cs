﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using LoremIpsumCMS.Infrastructure.Extensions;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Module.Admin.Areas.SiteAdmin.ViewModels.SiteAdmin;
using LoremIpsumCMS.Web.Infrastructure.Extensions;

namespace LoremIpsumCMS.Module.Admin.Areas.SiteAdmin.Validators.SiteAdmin
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractCreateOrEditViewModelValidator<T> : AbstractValidator<T>
        where T : CreateViewModel
    {

        #region Dependencies

        /// <summary>
        /// 
        /// </summary>
        private readonly IHttpContextAccessor _httpContextAccessor;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public AbstractCreateOrEditViewModelValidator(IHttpContextAccessor httpContextAxxessor)
        {
            _httpContextAccessor = httpContextAxxessor;

            Property(x => x.Name)
                .NotNull()
                .WithMessage("Name is required");
            Property(x => x.Hosts)
                .Validate();
            Property(x => x.ExtensionProperties)
                .All(x => x.PolymorphicValidate());

            ValidateUsing(ValidateHostnames);
        }

        #endregion

        #region Validate Hostnames

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private IEnumerable<ValidationMessage> ValidateHostnames(ValidationContext<T> context)
        {
            var hosts = context.Instance.Hosts
                .Where(x => !x.IsDeleted)
                .ToList();

            if (hosts.None())
            {
                yield return new ValidationMessage<T>(x => x.Hosts, "At least one host must be defined");
            }
            if (hosts.Any(x => x.Name == null))
            {
                yield return new ValidationMessage<T>(x => x.Hosts, "Hostname is required");
            }

            // Check that current request host is defined as admin
            var currentRequestHostIsAdmin = false;
            var currentRequestHostname = _httpContextAccessor.GetRequestHost().Host;
            foreach (var host in hosts)
            {
                if (host.Name == null)
                {
                    continue;
                }
                if (host.Name == currentRequestHostname && host.IsAdmin)
                {
                    currentRequestHostIsAdmin = true;
                }
            }
            if (!currentRequestHostIsAdmin)
            {
                yield return new ValidationMessage<T>(x => x.Hosts, "Current host is not defined as admin");
            }
        }

        #endregion

    }

}

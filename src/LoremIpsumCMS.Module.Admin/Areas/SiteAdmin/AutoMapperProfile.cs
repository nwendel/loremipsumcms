﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using AutoMapper;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Module.Admin.Areas.SiteAdmin.ViewModels.SiteAdmin;

namespace LoremIpsumCMS.Module.Admin.Areas.SiteAdmin
{

    /// <summary>
    /// 
    /// </summary>
    public class AutoMapperProfile : Profile
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public AutoMapperProfile()
        {
            CreateMap<SiteHostViewModel, SiteHost>();

            CreateMap<SiteHost, SiteHostViewModel>()
                .ForMember(x => x.IsDeleted, o => o.Ignore());
        }

        #endregion

    }

}

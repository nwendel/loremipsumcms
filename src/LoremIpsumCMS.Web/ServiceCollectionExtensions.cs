﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.DependencyInjection;
using FluentRegistration;
using LoremIpsumCMS.Infrastructure.Dependency;
using LoremIpsumCMS.Infrastructure.Extensions;
using LoremIpsumCMS.Web.Infrastructure.Mvc;

namespace LoremIpsumCMS.Web
{

    /// <summary>
    /// 
    /// </summary>
    public static class ServiceCollectionExtensions
    {

        #region Use Lorem Ipsum

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        public static void AddLoremIpsum(this IServiceCollection self)
        {
            if(self.None(x => x.ServiceType == typeof(IControllerFactory)))
            {
                throw new LoremIpsumException("AddMvc must be called before AddLoremIpsum");
            }

            // TODO: Use another type to identify this assembly
            self.Install(AllInstallers
                .FromAssemblyContaining<LoremIpsumException>()
                .OrderByDependencies()
                .ToArray());

            // TODO: Use another type to identify this assembly
            self.Install(AllInstallers
                .FromAssemblyContaining<AspMvcServiceInstaller>()
                .OrderByDependencies()
                .ToArray());
        }

        #endregion

    }

}



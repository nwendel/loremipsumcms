﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using LoremIpsumCMS.Web.Infrastructure.Initializer;
using LoremIpsumCMS.Infrastructure.Raven;

namespace LoremIpsumCMS.Web
{

    /// <summary>
    /// 
    /// </summary>
    public static class ApplicationBuilderExtensions
    {

        #region Use Lorem Ipsum

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        public static IApplicationBuilder UseLoremIpsum(this IApplicationBuilder self)
        {
            var serviceProvider = self.ApplicationServices;
            var initializerService = serviceProvider.GetRequiredService<IInitializerService>();
            var documentSessionHolder = serviceProvider.GetRequiredService<IDocumentSessionHolder>();

            using(new DocumentSessionScope(documentSessionHolder))
            {
                initializerService.ExecuteInitializers();
                initializerService.ExecuteApplicationBuilderInitializers(self);

                documentSessionHolder.Session.SaveChanges();
            }

            return self;
        }

        #endregion

    }

}

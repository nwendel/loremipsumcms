﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Initializer;
using LoremIpsumCMS.Services;
using System.Linq;

namespace LoremIpsumCMS.Web.Mvc.ModelBinding
{

    /// <summary>
    /// 
    /// </summary>
    public class TypeIdentifierCacheInitializer : IInitializer
    {

        #region Dependencies

        private readonly IContentTypeService _contentTypeService;
        private readonly TypeIdentifierCache _typeIdentifierCache;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentTypeService"></param>
        public TypeIdentifierCacheInitializer(
            IContentTypeService contentTypeService,
            TypeIdentifierCache typeIdentifierCache)
        {
            _contentTypeService = contentTypeService;
            _typeIdentifierCache = typeIdentifierCache;
        }

        #endregion

        #region Initialize

        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            var types = _contentTypeService.FindSiteExtensionProperties()
                .Concat(_contentTypeService.FindPages())
                .Concat(_contentTypeService.FindPageExtensionProperties())
                .ToList();
            foreach(var type in types)
            {
                _typeIdentifierCache.Register(type);
            }
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Services;

namespace LoremIpsumCMS.Web.Mvc.Filters
{

    // TODO: I should probably change this into a model binder

    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    public class SiteAwareAttribute : ActionFilterAttribute
    {

        #region On Action Executing

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            // TODO: This needs to be rewritten
            var parameters = context.ActionDescriptor.Parameters;
            var siteParameter = parameters.SingleOrDefault(x => x.ParameterType == typeof(Site));
            var siteReferenceParameter = parameters.SingleOrDefault(x => x.ParameterType == typeof(SiteReference));

            if (siteParameter != null || siteReferenceParameter != null)
            {
                var serviceProvider = context.HttpContext.RequestServices;
                var pageRouteService = serviceProvider.GetRequiredService<IPageRouteService>();

                var host = context.HttpContext.Request.Host.Host;
                var site = pageRouteService.GetSiteFromHost(host);

                if (siteParameter != null)
                {
                    var parameterName = siteParameter.Name;
                    var arguments = context.ActionArguments;
                    arguments[parameterName] = site;
                }
                if(siteReferenceParameter != null)
                {
                    var parameterName = siteReferenceParameter.Name;
                    var arguments = context.ActionArguments;
                    var siteReference = site != null
                        ? new SiteReference(site.Id, site.Name)
                        : null;
                    arguments[parameterName] = siteReference;
                }
            }

            base.OnActionExecuting(context);
        }


        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Microsoft.AspNetCore.Html;
using LoremIpsumCMS.Model;

namespace LoremIpsumCMS.Web.Mvc.Rendering
{

    /// <summary>
    /// 
    /// </summary>
    public static class LoremIpsumHelperPropertyForExtensions
    {

        #region Property For

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="self"></param>
        /// <param name="property"></param>
        /// <returns></returns>
        public static IHtmlContent PropertyFor<TModel>(
            this LoremIpsumHelper<TModel> self,
            Func<TModel, string> property)
            where TModel : AbstractContentData
        {
            var viewContext = self.ViewContext;
            var model = (TModel)viewContext.ViewData.Model;
            var value = property(model);
            return new HtmlString(value);
        }

        #endregion

    }

}

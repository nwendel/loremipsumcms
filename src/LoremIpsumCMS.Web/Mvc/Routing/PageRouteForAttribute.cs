﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Raven.Client;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Services;
using LoremIpsumCMS.Web.Mvc.Routing.Internal;
using System.Reflection;

namespace LoremIpsumCMS.Web.Mvc.Routing
{

    /// <summary>
    /// 
    /// </summary>
    public class PageRouteForAttribute : ActionFilterAttribute
    {

        // TODO: Move parameter handling to model binder?

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        public PageRouteForAttribute(Type dataType)
        {
            if(dataType == null)
            {
                throw new ArgumentNullException(nameof(dataType));
            }
            if(!typeof(AbstractPageData).GetTypeInfo().IsAssignableFrom(dataType))
            {
                throw new ArgumentException($"{dataType.FullName} must inherit from {typeof(AbstractPageData).FullName}", nameof(dataType));
            }

            DataType = dataType;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public Type DataType { get; private set; }

        #endregion

        #region On Action Executing

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var serviceProvider = context.HttpContext.RequestServices;
            var pageRouteService = serviceProvider.GetRequiredService<IPageRouteService>();
            var documentSession = serviceProvider.GetRequiredService<IDocumentSession>();
            var pageRouteInfoCache = serviceProvider.GetRequiredService<IPageRouteInfoCache>();

            var host = context.HttpContext.Request.Host.Host;
            var path = context.HttpContext.Request.Path;

            var site = pageRouteService.GetSiteFromHost(host);
            var page = documentSession.LoadBy<Page>(x => x.BySiteIdAndPath(site.Id, path));
            var routeInfo = pageRouteInfoCache.GetRouteInfo(page.Data.GetType());

            foreach(var parameter in routeInfo.Parameters)
            {
                context.ActionArguments[parameter.Name] = parameter.GetValue(site.Id, page.Id);
            }

            base.OnActionExecuting(context);
        }

        #endregion

    }

}
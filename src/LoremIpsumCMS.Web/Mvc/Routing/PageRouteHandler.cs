﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Internal;
using Microsoft.AspNetCore.Routing;
using Raven.Client;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Services;
using LoremIpsumCMS.Web.Mvc.Routing.Internal;

namespace LoremIpsumCMS.Web.Mvc.Routing
{

    /// <summary>
    /// 
    /// </summary>
    public class PageRouteHandler : IRouter
    {

        #region Dependencies

        private readonly IPageRouteService _pageRouteService;
        private readonly IPageRouteInfoCache _pageRouteInfoCache;
        private readonly IDocumentSession _documentSession;
        private readonly MvcRouteHandler _mvcRouteHandler;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageRouteService"></param>
        /// <param name="pageRouteInfoCache"></param>
        /// <param name="documentSession"></param>
        /// <param name="mvcRouteHandler"></param>
        public PageRouteHandler(
            IPageRouteService pageRouteService,
            IPageRouteInfoCache pageRouteInfoCache,
            IDocumentSession documentSession,
            MvcRouteHandler mvcRouteHandler)
        {
            _pageRouteService = pageRouteService;
            _pageRouteInfoCache = pageRouteInfoCache;
            _documentSession = documentSession;
            _mvcRouteHandler = mvcRouteHandler;
        }

        #endregion

        #region Route Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public Task RouteAsync(RouteContext context)
        {
            var host = context.HttpContext.Request.Host.Host;
            var path = context.HttpContext.Request.Path;

            var site = _pageRouteService.GetSiteFromHost(host);
            if (site == null)
            {
                return Task.CompletedTask;
            }
            var page = _documentSession.LoadBy<Page>(x => x.BySiteIdAndPath(site.Id, path));
            if(page == null)
            {
                return Task.CompletedTask;
            }

            var dataType = page.Data.GetType();
            var routeInfo = _pageRouteInfoCache.GetRouteInfo(dataType);
            context.RouteData.Values["controller"] = routeInfo.ControllerName;
            context.RouteData.Values["action"] = routeInfo.ActionName;

            // TODO: Handle area too

            return _mvcRouteHandler.RouteAsync(context);
        }

        #endregion

        #region Get Virtual Path

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public VirtualPathData GetVirtualPath(VirtualPathContext context)
        {
            return null;
        }

        #endregion

    }

}

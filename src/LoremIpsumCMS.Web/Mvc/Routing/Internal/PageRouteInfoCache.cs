﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using LoremIpsumCMS.Infrastructure.Extensions;
using LoremIpsumCMS.Infrastructure.Initializer;
using LoremIpsumCMS.Infrastructure.Reflection;

namespace LoremIpsumCMS.Web.Mvc.Routing.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public class PageRouteInfoCache : IPageRouteInfoCache, IInitializer
    {

        #region Dependencies

        private readonly ILogger _logger;
        private readonly IEnumerable<IRouteParameterFactory> _routeParameterFactories;

        #endregion

        #region Fields

        private bool _isInitialized;
        private readonly IDictionary<Type, PageRouteInfo> _pageRouteInfos = new Dictionary<Type, PageRouteInfo>();

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="routeParameterFactories"></param>
        public PageRouteInfoCache(
            ILogger<PageRouteInfoCache> logger,
            IEnumerable<IRouteParameterFactory> routeParameterFactories)
        {
            _logger = logger;
            _routeParameterFactories = routeParameterFactories;
        }

        #endregion

        #region Initialize

        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            var allTypes = ReflectionUtility.FindApplicationAndLoremIpsumAssemblies()
                .SelectMany(x => x.GetTypes())
                .ToList();
            var controllerTypes = allTypes
                .Where(x => !x.GetTypeInfo().IsAbstract && typeof(Controller).IsAssignableFrom(x));

            foreach(var controllerType in controllerTypes)
            {
                foreach(var methodInfo in controllerType.GetMethods())
                {
                    if(IsPageRoute(methodInfo))
                    {
                        var dataType = methodInfo.GetCustomAttribute<PageRouteForAttribute>().DataType;
                        var actionName = methodInfo.Name;
                        var routeParameters = AnalyzeRouteParameters(methodInfo, dataType);

                        _logger.LogDebug("Found page controller action");
                        _logger.LogDebug("DataType: {0}", dataType.Name);
                        _logger.LogDebug("ControllerType: {0}", controllerType.Name);
                        _logger.LogDebug("ActionName: {0}", actionName);

                        _pageRouteInfos[dataType] = new PageRouteInfo
                        {
                            ControllerName = controllerType.Name.TrimEndsWith(nameof(Controller)),
                            ActionName = methodInfo.Name,
                            Parameters = routeParameters
                        };
                    }
                }
            }
            _isInitialized = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <returns></returns>
        private static bool IsPageRoute(MethodInfo methodInfo)
        {
            var isPageRoute = methodInfo.IsPublic &&
                              typeof(IActionResult).IsAssignableFrom((methodInfo.ReturnParameter.ParameterType)) &&
                              methodInfo.GetCustomAttribute<PageRouteForAttribute>() != null;
            return isPageRoute;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <param name="dataType"></param>
        private IEnumerable<RouteParameter> AnalyzeRouteParameters(MethodInfo methodInfo, Type dataType)
        {
            var parameters = new List<RouteParameter>();
            var methodParameters = methodInfo.GetParameters();
            foreach(var methodParameter in methodParameters)
            {
                var methodParameterType = methodParameter.ParameterType;
                var factory = _routeParameterFactories.SingleOrDefault(x => x.CanCreate(methodParameterType, dataType));
                if(factory != null)
                {
                    var parameter = factory.Create(methodParameter.Name, methodParameterType, dataType);
                    parameters.Add(parameter);
                }
            }
            return parameters;
        }

        #endregion

        #region Get Route Info

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataType"></param>
        /// <returns></returns>
        public PageRouteInfo GetRouteInfo(Type dataType)
        {
            if(!_isInitialized)
            {
                throw new InvalidOperationException("PageRouteInfoCache is not initialized");
            }
            if(dataType == null)
            {
                throw new ArgumentNullException(nameof(dataType));
            }

            PageRouteInfo pageRouteInfo;
            var found = _pageRouteInfos.TryGetValue(dataType, out pageRouteInfo);
            if(!found)
            {
                throw new ArgumentException($"No route info found for {dataType.FullName}");
            }
            return _pageRouteInfos[dataType];
        }

        #endregion

    }

}

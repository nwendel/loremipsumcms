﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.Extensions.DependencyInjection;
using FluentRegistration;
using LoremIpsumCMS.Infrastructure.Dependency;
using LoremIpsumCMS.Infrastructure.Initializer;
using LoremIpsumCMS.Web.Infrastructure.Module;

namespace LoremIpsumCMS.Web.Infrastructure.Initializer
{
    /// <summary>
    /// 
    /// </summary>
    [DependsOn(typeof(ModuleServiceInstaller))]
    public class InitializerServiceInstaller : IServiceInstaller
    {

        #region Install

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        public void Install(IServiceCollection serviceCollection)
        {
            serviceCollection.Register(Component
                .For<IInitializerService>()
                .ImplementedBy<InitializerService>());

            serviceCollection.Register(AllClasses
                .FromAssemblyContaining<LoremIpsumException>()
                .BasedOn<IInitializer>()
                .WithService.AllInterfaces());

            serviceCollection.Register(AllClasses
                .FromAssemblyContaining<InitializerServiceInstaller>()
                .BasedOn<IInitializer>()
                .WithService.AllInterfaces());

            serviceCollection.Register(AllClasses
                .FromAssemblyContaining<InitializerServiceInstaller>()
                .BasedOn<IApplicationBuilderInitializer>()
                .WithService.AllInterfaces());

            InstallModuleInitializers(serviceCollection);
        }

        #endregion

        #region Install Module Initializers

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        public void InstallModuleInitializers(IServiceCollection serviceCollection)
        {
            var modules = serviceCollection.GetModules();
            foreach(var module in modules)
            {
                serviceCollection.Register(AllClasses
                    .FromAssembly(module.Assembly)
                    .BasedOn<IInitializer>()
                    .WithService.AllInterfaces());

                serviceCollection.Register(AllClasses
                    .FromAssembly(module.Assembly)
                    .BasedOn<IApplicationBuilderInitializer>()
                    .WithService.AllInterfaces());
            }
        }

        #endregion

    }

}

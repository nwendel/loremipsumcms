﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Microsoft.AspNetCore.Http;
using FluentRegistration;
using Raven.Client;
using LoremIpsumCMS.Infrastructure.Proxy;
using LoremIpsumCMS.Infrastructure.Raven;

namespace LoremIpsumCMS.Web.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public class DocumentSessionServiceFactory : 
        IServiceFactory<IDocumentSession>,
        IServiceFactory<IAsyncDocumentSession>
    {

        #region Dependencies

        private readonly IDocumentStore _documentStore;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IDocumentSessionHolder _documentSessionHolder;

        #endregion

        #region Fields

        private Lazy<IDocumentSession> _documentSession;
        private Lazy<IAsyncDocumentSession> _asyncDocumentSession;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentStore"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="documentSessionHolder"></param>
        public DocumentSessionServiceFactory(
            IDocumentStore documentStore,
            IHttpContextAccessor httpContextAccessor,
            IDocumentSessionHolder documentSessionHolder)
        {
            _documentStore = documentStore;
            _httpContextAccessor = httpContextAccessor;
            _documentSessionHolder = documentSessionHolder;

            _documentSession = new Lazy<IDocumentSession>(CreateDocumentSessionLazy);
            _asyncDocumentSession = new Lazy<IAsyncDocumentSession>(CreateAsyncDocumentSessionLazy);
        }

        #endregion

        #region Create

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IDocumentSession IServiceFactory<IDocumentSession>.Create()
        {
            return _documentSession.Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IAsyncDocumentSession IServiceFactory<IAsyncDocumentSession>.Create()
        {
            return _asyncDocumentSession.Value;
        }

        #endregion

        #region Create Document Session Lazy

        /// <summary>
        /// 
        /// </summary>
        public IDocumentSession CreateDocumentSessionLazy()
        {
            var lazyDocumentSession = ProxyFactory.Create(() =>
            {
                var httpContext = _httpContextAccessor.HttpContext;
                if(httpContext == null)
                {
                    return _documentSessionHolder.Session;
                }

                var documentSession = httpContext.GetDocumentSession();
                if (documentSession == null)
                {
                    documentSession = _documentStore.OpenSession("loremipsum");
                    httpContext.SetDocumentSession(documentSession);
                }
                return documentSession;
            });
            return lazyDocumentSession;
        }

        /// <summary>
        /// 
        /// </summary>
        public IAsyncDocumentSession CreateAsyncDocumentSessionLazy()
        {
            var asyncDocumentSessionLazy = ProxyFactory.Create(() =>
            {
                var httpContext = _httpContextAccessor.HttpContext;
                var asyncDocumentSession = httpContext.GetAsyncDocumentSession();
                if (asyncDocumentSession == null)
                {
                    asyncDocumentSession = _documentStore.OpenAsyncSession("loremipsum");
                    httpContext.SetAsyncDocumentSession(asyncDocumentSession);
                }
                return asyncDocumentSession;
            });
            return asyncDocumentSessionLazy;
        }

        #endregion

    }

}

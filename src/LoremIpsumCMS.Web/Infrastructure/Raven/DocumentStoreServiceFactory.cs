﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using FluentRegistration;
using Raven.Client;
using Raven.Client.Document;
using Raven.Client.UniqueConstraints;

namespace LoremIpsumCMS.Web.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public class DocumentStoreServiceFactory : IServiceFactory<IDocumentStore>
    {

        #region Fields

        private readonly Lazy<IDocumentStore> _documentStore = new Lazy<IDocumentStore>(CreateInternal);

        #endregion

        #region Create

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IDocumentStore Create()
        {
            return _documentStore.Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IDocumentStore CreateInternal()
        { 
            var documentStore = new DocumentStore
            {
                Url = "http://localhost:9090",
            };
            documentStore.RegisterListener(new UniqueConstraintsStoreListener());
            documentStore.Initialize();
            return documentStore;
        }

        #endregion

    }

}

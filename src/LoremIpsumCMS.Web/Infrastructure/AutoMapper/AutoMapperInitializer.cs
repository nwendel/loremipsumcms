﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using AutoMapper;
using LoremIpsumCMS.Infrastructure.Initializer;
using LoremIpsumCMS.Web.Infrastructure.Module;

namespace LoremIpsumCMS.Infrastructure.AutoMapper
{

    /// <summary>
    /// 
    /// </summary>
    public class AutoMapperInitializer : IInitializer
    {

        #region Dependencies

        private readonly IEnumerable<IModule> _modules;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modules"></param>
        public AutoMapperInitializer(IEnumerable<IModule> modules)
        {
            _modules = modules;
        }

        #endregion

        #region Initialize

        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfiles(typeof(LoremIpsumException), typeof(AutoMapperInitializer));
                foreach(var module in _modules)
                {
                    x.AddProfiles(module.Assembly);
                }
            });
            Mapper.AssertConfigurationIsValid();
        }

        #endregion

    }

}

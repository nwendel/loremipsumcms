﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Reflection;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using FluentRegistration;
using LoremIpsumCMS.Web.Infrastructure.Module;
using LoremIpsumCMS.Infrastructure.Dependency;

namespace LoremIpsumCMS.Web.Infrastructure.Mvc
{

    /// <summary>
    /// 
    /// </summary>
    [DependsOn(typeof(ModuleServiceInstaller))]
    public class RazorViewEngineOptionsServiceInstaller : IServiceInstaller
    {

        #region Install

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        public void Install(IServiceCollection serviceCollection)
        {
            serviceCollection.Configure<RazorViewEngineOptions>(o =>
            {
                var modules = serviceCollection.GetModules();
                foreach (var module in modules)
                {
                    var fileProvider = new EmbeddedFileProvider(module.Assembly, module.BaseNamespace);
                    o.FileProviders.Add(fileProvider);
                }
            });
        }

        #endregion

    }

}

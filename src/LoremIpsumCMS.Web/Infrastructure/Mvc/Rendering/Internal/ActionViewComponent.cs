﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Internal;
using Microsoft.AspNetCore.Routing;

namespace LoremIpsumCMS.Web.Infrastructure.Mvc.Rendering.Internal
{

    /// <summary>
    /// 
    /// </summary>
    [ViewComponent]
    public class ActionViewComponent : ViewComponent
    {

        #region Dependencies

        private readonly MvcRouteHandler _mvcRouteHandler;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IActionSelector _actionSelector;
        private readonly IControllerFactory _controllerFactory;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mvcRouteHandler"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="actionSelector"></param>
        /// <param name="controllerFactory"></param>
        public ActionViewComponent(
            MvcRouteHandler mvcRouteHandler,
            IHttpContextAccessor httpContextAccessor,
            IActionSelector actionSelector,
            IControllerFactory controllerFactory)
        {
            _mvcRouteHandler = mvcRouteHandler;
            _httpContextAccessor = httpContextAccessor;
            _actionSelector = actionSelector;
            _controllerFactory = controllerFactory;
        }

        #endregion

        #region Invoke Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="controllerName"></param>
        /// <param name="actionName"></param>
        /// <param name="routeValues"></param>
        /// <returns></returns>
        public async Task<IHtmlContent> InvokeAsync(string controllerName, string actionName, object routeValues)
        {
            // TODO: What to do instead?
            await Task.Run(() => { });

            var httpContext = _httpContextAccessor.HttpContext;
            var routeContext = new RouteContext(httpContext);

            routeContext.RouteData.Values["controller"] = controllerName;
            routeContext.RouteData.Values["action"] = actionName;

            var rvd = new RouteValueDictionary(routeValues);
            if (rvd.ContainsKey("Area"))
            {
                routeContext.RouteData.Values["area"] = rvd["Area"];
            }

            var candidates = _actionSelector.SelectCandidates(routeContext);
            var actionDescriptor = _actionSelector.SelectBestCandidate(routeContext, candidates);
            var actionContext = new ActionContext(routeContext.HttpContext, routeContext.RouteData, actionDescriptor);

            var controllerContext = new ControllerContext(actionContext);
            var controller = _controllerFactory.CreateController(controllerContext);

            var controllerActionDescriptor = (ControllerActionDescriptor)actionDescriptor;
            var executor = ObjectMethodExecutor.Create(controllerActionDescriptor.MethodInfo, controllerActionDescriptor.ControllerTypeInfo);

            var actionResult = (IActionResult)executor.Execute(controller, null);
            _controllerFactory.ReleaseController(controllerContext, controller);

            return new ActionViewComponentResult(actionContext, actionResult);
        }

        #endregion

    }

}

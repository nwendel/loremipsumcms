﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.IO;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;

namespace LoremIpsumCMS.Web.Infrastructure.Mvc.Rendering.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public class ActionViewComponentResult : IHtmlContent
    {

        #region Fields

        private readonly ActionContext _actionContext;
        private readonly IActionResult _actionResult;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionResult"></param>
        public ActionViewComponentResult(ActionContext actionContext, IActionResult actionResult)
        {
            _actionContext = actionContext;
            _actionResult = actionResult;
        }

        #endregion

        #region Write To

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="encoder"></param>
        public void WriteTo(TextWriter writer, HtmlEncoder encoder)
        {
            var oldBody = _actionContext.HttpContext.Response.Body;
            try
            {
                var memoryStream = new MemoryStream();
                _actionContext.HttpContext.Response.Body = memoryStream;
                _actionResult.ExecuteResultAsync(_actionContext).Wait();

                memoryStream.Position = 0;
                var reader = new StreamReader(memoryStream);
                var content = reader.ReadToEnd();
                writer.Write(content);
            }
            finally
            {
                _actionContext.HttpContext.Response.Body = oldBody;
            }
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Reflection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Primitives;

namespace LoremIpsumCMS.Web.Infrastructure.Mvc
{

    /// <summary>
    /// 
    /// </summary>
    public class FixedEmbeddedFileProvider : IFileProvider
    {

        private readonly IFileProvider _fileProvider;

        /// <summary>
        /// 
        /// </summary>
        /// <param name=""></param>
        //public FixedEmbeddedFileProvider(Assembly assembly)
        //{
        //    _fileProvider = new EmbeddedFileProvider(assembly);
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="assembly"></param>
        /// <param name="baseNamespace"></param>
        public FixedEmbeddedFileProvider(Assembly assembly, string baseNamespace)
        {
            _fileProvider = new EmbeddedFileProvider(assembly, baseNamespace);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subpath"></param>
        /// <returns></returns>
        public IDirectoryContents GetDirectoryContents(string subpath)
        {
            throw new NotSupportedException();
            //return _fileProvider.GetDirectoryContents(subpath);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subpath"></param>
        /// <returns></returns>
        public IFileInfo GetFileInfo(string subpath)
        {
            var directoryName = subpath.Substring(0, subpath.LastIndexOf("/"));
            var fileName = subpath.Substring(subpath.LastIndexOf("/"));
            var fixedSubpath = directoryName.Replace('-', '_') + fileName;
            return _fileProvider.GetFileInfo(fixedSubpath);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IChangeToken Watch(string filter)
        {
            throw new NotSupportedException();
            //return _fileProvider.Watch(filter);
        }

    }

}

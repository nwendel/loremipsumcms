﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using LoremIpsumCMS.Web.Infrastructure.Initializer;
using LoremIpsumCMS.Web.Infrastructure.Module;

namespace LoremIpsumCMS.Web.Infrastructure.Mvc
{

    /// <summary>
    /// 
    /// </summary>
    public class EmbeddedStaticFileInitializer : IApplicationBuilderInitializer
    {

        #region Dependencies

        private readonly IServiceProvider _serviceProvider;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceProvider"></param>
        public EmbeddedStaticFileInitializer(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        #endregion

        #region Initialize

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationBuilder"></param>
        public void Initialize(IApplicationBuilder applicationBuilder)
        {
            var modules = _serviceProvider.GetServices<IModule>();
            foreach (var module in modules)
            {
                applicationBuilder.UseStaticFiles(new StaticFileOptions
                {
                    FileProvider = new FixedEmbeddedFileProvider(module.Assembly, module.BaseNamespace + ".wwwroot")
                });
            }
        }

        #endregion

    }

}

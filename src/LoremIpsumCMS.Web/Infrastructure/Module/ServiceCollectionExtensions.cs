﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using AttachedProperties;
using LoremIpsumCMS.Infrastructure.Extensions;

namespace LoremIpsumCMS.Web.Infrastructure.Module
{

    /// <summary>
    /// 
    /// </summary>
    public static class ServiceCollectionExtensions
    {

        #region Get Modules

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static IEnumerable<IModule> GetModules(this IServiceCollection self)
        {
            var areModulesLoaded = self.GetAttachedValue(AttachedProperty.AreModulesLoaded);
            if(!areModulesLoaded)
            {
                throw new LoremIpsumException("GetModules called before modules are loaded");
            }
            var modules = self.GetAttachedValue(AttachedProperty.Modules);
            if(modules != null)
            {
                return modules;
            }

            var serviceProvider = self.BuildServiceProvider();
            modules = serviceProvider.GetServices<IModule>();
            self.SetAttachedValue(AttachedProperty.Modules, modules);
            //self.UpdateWithResolvedSingletons(serviceProvider);
            return modules;
        }

        #endregion

    }

}

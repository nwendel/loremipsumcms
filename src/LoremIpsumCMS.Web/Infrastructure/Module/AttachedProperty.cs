﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using AttachedProperties;

namespace LoremIpsumCMS.Web.Infrastructure.Module
{

    /// <summary>
    /// 
    /// </summary>
    public static class AttachedProperty
    {

        /// <summary>
        /// 
        /// </summary>
        public static AttachedProperty<IServiceCollection, bool> AreModulesLoaded = new AttachedProperty<IServiceCollection, bool>(nameof(AreModulesLoaded));

        /// <summary>
        /// 
        /// </summary>
        public static AttachedProperty<IServiceCollection, IEnumerable<IModule>> Modules = new AttachedProperty<IServiceCollection, IEnumerable<IModule>>(nameof(Modules));

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace LoremIpsumCMS.Web.Infrastructure.Module
{

    /// <summary>
    /// 
    /// </summary>
    public interface IModule
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        string Name { get; }

        /// <summary>
        /// 
        /// </summary>
        Assembly Assembly { get; }

        /// <summary>
        /// 
        /// </summary>
        string BaseNamespace { get; }

        #endregion

        #region Configure Services

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        void ConfigureServices(IServiceCollection serviceCollection);

        #endregion

        #region Configure

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationBuilder"></param>
        void Configure(IApplicationBuilder applicationBuilder);

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using AttachedProperties;
using FluentRegistration;
using LoremIpsumCMS.Infrastructure.Extensions;
using LoremIpsumCMS.Infrastructure.Dependency;

namespace LoremIpsumCMS.Web.Infrastructure.Module
{

    /// <summary>
    /// 
    /// </summary>
    public class ModuleServiceInstaller : IServiceInstaller
    {

        #region Install

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        public void Install(IServiceCollection serviceCollection)
        {
            serviceCollection.Register(Component
                .For<IModuleLoader>()
                .ImplementedBy<ModuleLoader>());

            var serviceProvider = serviceCollection.BuildServiceProvider();
            var moduleTypes = LoadModules(serviceProvider);
            ConfigureModuleServices(serviceCollection, moduleTypes);

            //serviceCollection.UpdateWithResolvedSingletons(serviceProvider);
            serviceCollection.SetAttachedValue(AttachedProperty.AreModulesLoaded, true);
        }

        #endregion

        #region Load Modules

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceProvider"></param>
        private static IEnumerable<Type> LoadModules(IServiceProvider serviceProvider)
        {
            var moduleLoader = serviceProvider.GetRequiredService<IModuleLoader>();
            var moduleTypes = moduleLoader.LoadAll();
            return moduleTypes;
        }

        #endregion

        #region Configure Module Services

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        /// <param name="moduleTypes"></param>
        private static void ConfigureModuleServices(IServiceCollection serviceCollection, IEnumerable<Type> moduleTypes)
        {
            var modules = moduleTypes
                .Select(x => Activator.CreateInstance(x))
                .Cast<IModule>()
                .OrderByDependencies()
                .ToList();
            foreach (var module in modules)
            {
                serviceCollection.Register(Component
                    .For<IModule>()
                    .Instance(module));
                module.ConfigureServices(serviceCollection);
            }
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using Microsoft.AspNetCore.Routing;
using LoremIpsumCMS.Infrastructure.Extensions;
using LoremIpsumCMS.Infrastructure.Validation;

namespace LoremIpsumCMS.Web.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidationResult
    {

        #region Fields

        private readonly List<ValidationMessage> _messages = new List<ValidationMessage>();

        #endregion

        #region Static Results

        /// <summary>
        /// 
        /// </summary>
        public static readonly ValidationResult Success = new ValidationResult();

        /// <summary>
        /// 
        /// </summary>
        public static readonly ValidationResult NotFound = new ValidationResult { RedirectResult = new NotFoundResult() };

        /// <summary>
        /// 
        /// </summary>
        public static readonly ValidationResult Forbidden = new ValidationResult { RedirectResult = new ForbidResult() };

        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionName"></param>
        /// <returns></returns>
        public static ValidationResult RedirectToAction(string actionName)
        {
            var rvd = new RouteValueDictionary(new { action = actionName });
            var validationResult = new ValidationResult
            {
                RedirectResult = new RedirectToRouteResult(rvd)
            };
            return validationResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static ValidationResult Message(string text, params object[] args)
        {
            var result = new ValidationResult();
            result.AddMessage(text, args);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyExpression"></param>
        /// <param name="text"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static ValidationResult Message<T>(Expression<Func<T, object>> propertyExpression, string text, params object[] args)
        {
            var result = new ValidationResult();
            result.AddMessage(propertyExpression, text, args);
            return result;
        }

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        private ValidationResult()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messages"></param>
        public ValidationResult(IEnumerable<ValidationMessage> messages)
        {
            messages.Each(x => _messages.Add(x));
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public bool IsSuccess => _messages.None();

        /// <summary>
        /// 
        /// </summary>
        public bool HasMessages => _messages.Any();

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<ValidationMessage> Messages => _messages.AsReadOnly();

        /// <summary>
        /// 
        /// </summary>
        public bool IsRedirect => RedirectResult != null;

        /// <summary>
        /// 
        /// </summary>
        public ActionResult RedirectResult { get; private set; }

        #endregion

        #region Add Message

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="args"></param>
        private void AddMessage(string text, params object[] args)
        {
            var message = new ValidationMessage(string.Empty, text, args);
            _messages.Add(message);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyExpression"></param>
        /// <param name="text"></param>
        /// <param name="args"></param>
        private void AddMessage<T>(Expression<Func<T, object>> propertyExpression, string text, params object[] args)
        {
            var expression = (LambdaExpression)propertyExpression;
            if (propertyExpression.Body.NodeType == ExpressionType.Convert)
            {
                expression = Expression.Lambda((propertyExpression.Body as UnaryExpression).Operand);
            }
            var propertyName = ExpressionHelper.GetExpressionText(expression);

            var message = new ValidationMessage(propertyName, text, args);
            _messages.Add(message);
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using LoremIpsumCMS.Infrastructure.Extensions;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Web.Infrastructure.Extensions;

namespace LoremIpsumCMS.Web.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidateControllerActionAttribute : ActionFilterAttribute
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="validationService"></param>
        public ValidateControllerActionAttribute()
        {
            Order = int.MaxValue;
        }

        #endregion

        #region On Action Executing

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            Validate(context);
            base.OnActionExecuting(context);
        }

        #endregion

        #region On Action Execution Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="next"></param>
        /// <returns></returns>
        public override Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            // TODO: This uses non-async method internally, no need to call validate here too?
            // TODO: Review is that always true? what if I have an actual async controller method?
            // Validate(context);
            return base.OnActionExecutionAsync(context, next);
        }

        #endregion

        #region Validate

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void Validate(ActionExecutingContext context)
        {
            var actionDescriptor = context.ActionDescriptor;
            var actionName = actionDescriptor.RouteValues["action"];
            var actionParameters = actionDescriptor.Parameters;
            var actionParameterTypes = actionParameters.Select(x => x.ParameterType).ToArray();
            var actionArgumentValues = context.ActionArguments.Values.ToArray();
            var controller = (Controller)context.Controller;
            var controllerType = controller.GetType();
            var serviceProvider = context.HttpContext.RequestServices;

            ValidateArguments(controller, actionArgumentValues, serviceProvider);
            var result = InvokeControllerValidate(controller, controllerType, actionName, actionParameterTypes, actionArgumentValues);
            if (result.IsRedirect)
            {
                context.Result = result.RedirectResult;
            }
            else if (!controller.ModelState.IsValid && !result.IsRedirect)
            {
                InvokeControllerInvalid(context, controller, controllerType, actionName, actionParameterTypes, actionArgumentValues);
            }
        }

        #endregion

        #region Validate Arguments

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void ValidateArguments(Controller controller, object[] actionArgumentValues, IServiceProvider serviceProvider)
        {
            var validationService = serviceProvider.GetRequiredService<IValidationService>();
            foreach (var argument in actionArgumentValues.Where(x => x != null))
            {
                var messages = validationService.Validate(argument);
                controller.ModelState.AddModelErrors(messages);
            }
        }

        #endregion

        #region Invoke Controller Validate

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private ValidationResult InvokeControllerValidate(
            Controller controller, Type controllerType, 
            string actionName, Type[] actionParameterTypes, object[] actionArgumentValues)
        {
            var validateMethodInfo = controllerType.GetMethod("Validate" + actionName, actionParameterTypes);
            if (validateMethodInfo == null)
            {
                return ValidationResult.Success;
            }

            var result = (ValidationResult)validateMethodInfo.InvokeAndUnwrapException(controller, actionArgumentValues);
            if (result.HasMessages)
            {
                controller.ModelState.AddModelErrors(result.Messages);
            }
            return result;
        }

        #endregion

        #region Invoke Controller Invalid

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void InvokeControllerInvalid(
            ActionExecutingContext context, Controller controller, Type controllerType,
            string actionName, Type[] actionParameterTypes, object[] actionArgumentValues)
        {
            var invalidMethodInfo = controllerType.GetMethod("Invalid" + actionName, actionParameterTypes);
            if (invalidMethodInfo == null)
            {
                throw new ValidationMethodException($"Method Invalid{actionName} is missing from controller named {controllerType.FullName}");
            }

            var actionResult = (ActionResult)invalidMethodInfo.InvokeAndUnwrapException(controller, actionArgumentValues);
            if (actionResult == null)
            {
                throw new ValidationMethodException($"Method Invalid{actionName} cannot return null");
            }
            context.Result = actionResult;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Raven.Client.UniqueConstraints;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;

namespace LoremIpsumCMS.Queries
{

    /// <summary>
    /// 
    /// </summary>
    public static class PageLoadByExtensions
    {

        #region By Site Id And Path

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="nameSlug"></param>
        /// <returns></returns>
        public static Page BySiteIdAndPath(this RavenSessionLoadBy<Page> self, string siteId, string path)
        {
            if (string.IsNullOrEmpty(siteId))
            {
                throw new ArgumentNullException(nameof(siteId));
            }
            if(string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException(nameof(path));
            }

            var page = self.Session.LoadByUniqueConstraint<Page>(x => x.SiteIdAndPath, $"{siteId}/{path}");
            return page;
        }

        #endregion

    }

}

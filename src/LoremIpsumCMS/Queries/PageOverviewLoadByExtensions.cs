﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Raven.Client.UniqueConstraints;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;

namespace LoremIpsumCMS.Queries
{

    /// <summary>
    /// 
    /// </summary>
    public static class PageOverviewLoadByExtensions
    {

        #region By Page Id

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="pageId"></param>
        /// <returns></returns>
        public static PageOverview ByPageId(this RavenSessionLoadBy<PageOverview> self, string pageId)
        {
            if (string.IsNullOrEmpty(pageId))
            {
                throw new ArgumentNullException(nameof(pageId));
            }

            var pageOverview = self.Session.LoadByUniqueConstraint<PageOverview>(x => x.PageId, pageId);
            return pageOverview;
        }

        #endregion

        #region By Site Id And Path

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="nameSlug"></param>
        /// <returns></returns>
        public static PageOverview BySiteIdAndPath(this RavenSessionLoadBy<PageOverview> self, string siteId, string path)
        {
            if (string.IsNullOrEmpty(siteId))
            {
                throw new ArgumentNullException(nameof(siteId));
            }
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException(nameof(path));
            }

            var pageOverview = self.Session.LoadByUniqueConstraint<PageOverview>(x => x.SiteIdAndPath, $"{siteId}/{path}");
            return pageOverview;
        }

        #endregion

    }

}

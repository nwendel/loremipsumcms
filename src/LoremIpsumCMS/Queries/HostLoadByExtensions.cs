#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Raven.Client.UniqueConstraints;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;

namespace LoremIpsumCMS.Queries
{

    /// <summary>
    /// 
    /// </summary>
    public static class HostLoadByExtensions
    {

        #region By Name

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Host ByName(this RavenSessionLoadBy<Host> self, string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name));
            }

            var site = self.Session.LoadByUniqueConstraint<Host>(x => x.Name, name);
            return site;
        }

        #endregion

    }

}
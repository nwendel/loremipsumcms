﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using LoremIpsumCMS.Infrastructure.Reflection;
using LoremIpsumCMS.Model;

namespace LoremIpsumCMS.Services
{

    /// <summary>
    /// 
    /// </summary>
    public class ContentTypeService : IContentTypeService
    {

        #region Find Site Extension Properties

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Type> FindSiteExtensionProperties()
        {
            return FindTypesBasedOn<AbstractSiteExtensionProperties>();
        }

        #endregion

        #region Find Pages

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Type> FindPages()
        {
            return FindTypesBasedOn<AbstractPageData>();
        }

        #endregion

        #region Find Page Extension Properties

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Type> FindPageExtensionProperties()
        {
            return FindTypesBasedOn<AbstractPageExtensionProperties>();
        }

        #endregion

        #region Find Types Based On

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private IEnumerable<Type> FindTypesBasedOn<T>()
        {
            var assemblies = ReflectionUtility.FindApplicationAndLoremIpsumAssemblies();
            var types = assemblies
                .SelectMany(x => x.GetTypes())
                .Where(x => typeof(T).IsAssignableFrom(x))
                .Where(x => !x.GetTypeInfo().IsAbstract)
                .Where(x => x.GetTypeInfo().GetGenericArguments().Length == 0)
                .ToList();
            return types;
        }

        #endregion

    }

}

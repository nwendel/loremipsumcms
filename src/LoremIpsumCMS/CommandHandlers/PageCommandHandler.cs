﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Cqrs;

namespace LoremIpsumCMS.CommandHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class PageCommandHandler :
        IHandleCommand<CreatePageCommand>,
        IHandleCommand<UpdatePageCommand>,
        IHandleCommand<DeletePageCommand>
    {

        #region Handle Create

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<AbstractEvent> Handle(CreatePageCommand command)
        {
            yield return new PageCreatedEvent
            {
                SiteId = command.SiteId,
                Path = command.Path,
                Title = command.Title,
                MetaKeywords = command.MetaKeywords,
                MetaDescription = command.MetaDescription,
                Data = command.Data,
                ExtensionProperties = command.ExtensionProperties,
                CreatedAt = command.CreatedAt
            };
        }

        #endregion

        #region Handle Update

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<AbstractEvent> Handle(UpdatePageCommand command)
        {
            yield return new PageUpdatedEvent
            {
                Id = command.Id,
                Path = command.Path,
                Title = command.Title,
                MetaKeywords = command.MetaKeywords,
                MetaDescription = command.MetaDescription,
                Data = command.Data,
                ExtensionProperties = command.ExtensionProperties,
                UpdatedAt = command.UpdatedAt
            };
        }

        #endregion

        #region Handle Delete

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<AbstractEvent> Handle(DeletePageCommand command)
        {
            yield return new PageDeletedEvent
            {
                Id = command.Id
            };
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Events;

namespace LoremIpsumCMS.CommandHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class SiteCommandHandler :
        IHandleCommand<CreateSiteCommand>,
        IHandleCommand<UpdateSiteCommand>
    {

        #region Handle Create

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<AbstractEvent> Handle(CreateSiteCommand command)
        {
            yield return new SiteCreatedEvent
            {
                Name = command.Name,
                Hosts = command.Hosts,
                ExtensionProperties = command.ExtensionProperties,
                CreatedAt = command.CreatedAt
            };
        }

        #endregion

        #region Handle Update

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<AbstractEvent> Handle(UpdateSiteCommand command)
        {
            yield return new SiteUpdatedEvent
            {
                Id = command.Id,
                Name = command.Name,
                Hosts = command.Hosts,
                ExtensionProperties = command.ExtensionProperties,
                UpdatedAt = command.UpdatedAt,
            };
        }

        #endregion

    }

}

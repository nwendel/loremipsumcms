﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Raven.Imports.Newtonsoft.Json;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Model;

namespace LoremIpsumCMS.Infrastructure.EventStore
{

    /// <summary>
    /// 
    /// </summary>
    public class PersistedEvent : AbstractRootEntity
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        protected PersistedEvent()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public PersistedEvent(AbstractEvent @event) : this()
        {
            if (@event == null)
            {
                throw new ArgumentNullException(nameof(@event));
            }

            At = DateTime.Now;
            Event = @event;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public DateTime At { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public string AggregateId
        {
            get { return Event.Id; }
        }

        /// <summary>
        /// 
        /// </summary>
        public AbstractEvent Event { get; protected set; }

        #endregion

    }

}

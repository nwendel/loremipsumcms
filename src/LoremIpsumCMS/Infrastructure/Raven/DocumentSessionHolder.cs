﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Raven.Client;

namespace LoremIpsumCMS.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public class DocumentSessionHolder : IDocumentSessionHolder
    {

        #region Dependencies

        private readonly IDocumentStore _documentStore;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentStore"></param>
        public DocumentSessionHolder(IDocumentStore documentStore)
        {
            _documentStore = documentStore;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public IDocumentSession Session { get; private set; }

        #endregion

        #region Open Session

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public void OpenSession()
        {
            Session = _documentStore.OpenSession("loremipsum");
        }

        #endregion

        #region Close Session
        
        /// <summary>
        /// 
        /// </summary>
        public void CloseSession()
        {
            Session.Dispose();
            Session = null;
        }

        #endregion

    }

}

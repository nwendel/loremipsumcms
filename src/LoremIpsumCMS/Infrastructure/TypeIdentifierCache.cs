﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Security.Cryptography;

namespace LoremIpsumCMS.Infrastructure
{

    /// <summary>
    /// 
    /// </summary>
    public class TypeIdentifierCache
    {

        #region Fields

        private readonly MD5 _md5 = MD5.Create();
        private readonly Dictionary<Type, string> _typeToIdentifier = new Dictionary<Type, string>();
        private readonly Dictionary<string, Type> _identifierToType = new Dictionary<string, Type>();

        #endregion

        #region Register

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        public void Register(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }
            if (type.GetTypeInfo().IsAbstract)
            {
                throw new ArgumentException("Cannot be abstract", nameof(type));
            }
            if (type.GetTypeInfo().IsGenericTypeDefinition)
            {
                throw new ArgumentException("Cannot be generic type definition", nameof(type));
            }

            var typeName = type.AssemblyQualifiedName;
            var stream = new StringStream(typeName);
            var hashBytes = _md5.ComputeHash(stream);
            var identifier = BitConverter.ToString(hashBytes).Replace("-", "");

            // TODO: Check duplicate registration
            // TODO: Check hash collisions

            _identifierToType.Add(identifier, type);
            _typeToIdentifier.Add(type, identifier);

        }

        #endregion

        #region Get Hash

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public string GetIdentifier(Type type)
        {
            // TODO: Check that it is registered
            return _typeToIdentifier[type];
        }

        #endregion

        #region Get Type

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public Type GetType(string identifier)
        {
            // TODO: Check that it is registered
            return _identifierToType[identifier];
        }

        #endregion

    }

}

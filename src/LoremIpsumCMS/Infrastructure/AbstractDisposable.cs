﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;

namespace LoremIpsumCMS.Infrastructure
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractDisposable : IDisposable
    {

        #region Fields

        private bool _disposed;

        #endregion

        #region Dispose

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                ManagedDispose();
            }
            UnmanagedDispose();

            _disposed = true;
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void ManagedDispose()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void UnmanagedDispose()
        {
        }

        #endregion

        #region Destructor

        /// <summary>
        /// 
        /// </summary>
        ~AbstractDisposable()
        {
            Dispose(false);
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using LoremIpsumCMS.Infrastructure.Validation.Internal;

namespace LoremIpsumCMS.Infrastructure.Validation.PropertyValidators
{

    /// <summary>
    /// 
    /// </summary>
    public class ArrayAllPropertyValidator<T, TItem> : IPropertyValidator<T, TItem[]>
    {

        #region Fields

        private readonly PropertyRule<TItem, TItem> _rule;
        private readonly RuleBuilder<TItem, TItem> _ruleBuilder;
        private readonly Action<RuleBuilder<TItem, TItem>> _itemValidatorAction;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemValidatorAction"></param>
        public ArrayAllPropertyValidator(Action<RuleBuilder<TItem, TItem>> itemValidatorAction)
        {
            _itemValidatorAction = itemValidatorAction;
            _rule = new PropertyRule<TItem, TItem>(x => x);
            _ruleBuilder = new RuleBuilder<TItem, TItem>(_rule);
            _itemValidatorAction(_ruleBuilder);
        }

        #endregion

        #region Validate

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        public IEnumerable<ValidationMessage> Validate(string propertyName, TItem[] value, IServiceProvider serviceProvider)
        {
            var safeValue = value ?? new TItem[0];
            var messages = safeValue.SelectMany((item, ix) =>
            {
                var context = new ValidationContext<TItem>(item, serviceProvider);
                var itemMessages = _rule.Validate(context);
                var adjustedItemMessages = itemMessages.Select(x => x.PropertyName == string.Empty
                    ? new ValidationMessage($"{propertyName}[{ix}]", $"Values for {propertyName}{x.Text}")
                    : new ValidationMessage($"{propertyName}[{ix}]{x.PropertyName}", x.Text));
                return adjustedItemMessages;
            });
            return messages;
        }

        #endregion

    }

}

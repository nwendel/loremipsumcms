﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LoremIpsumCMS.Infrastructure.Validation.Internal;

namespace LoremIpsumCMS.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class AbstractValidator<T> : IValidator<T>, IValidator
    {

        #region Fields

        private readonly IList<IPropertyRule<T>> _rules = new List<IPropertyRule<T>>();

        #endregion

        #region Validate

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        IEnumerable<ValidationMessage> IValidator.Validate(ValidationContext<object> context)
        {
            var this_ = (IValidator<T>)this;
            var context_ = new ValidationContext<T>((T)context.Instance, context.ServiceProvider);
            var messages = this_.Validate(context_);
            return messages;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public IEnumerable<ValidationMessage> Validate(ValidationContext<T> context)
        {
            var messages = _rules.SelectMany(x => x.Validate(context));
            return messages;
        }

        #endregion

        #region Property

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyExpression"></param>
        /// <returns></returns>
        protected RuleBuilder<T, TResult> Property<TResult>(Expression<Func<T, TResult>> propertyExpression)
        {
            var rule = new PropertyRule<T, TResult>(propertyExpression);
            _rules.Add(rule);
            var builder = new RuleBuilder<T, TResult>(rule);
            return builder;
        }

        #endregion

        #region Validate Using

        /// <summary>
        /// 
        /// </summary>
        /// <param name="validator"></param>
        protected IRuleBuilderOptions ValidateUsing(Func<ValidationContext<T>, IEnumerable<ValidationMessage>> validator)
        {
            var rule = new PropertyRule<T, T>(x => x);
            _rules.Add(rule);
            var builder = new RuleBuilder<T, T>(rule);
            builder.ValidateUsing(validator);
            return builder;
        }

        #endregion

    }

}

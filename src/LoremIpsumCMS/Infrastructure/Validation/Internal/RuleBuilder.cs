﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using LoremIpsumCMS.Infrastructure.Validation.PropertyValidators;

namespace LoremIpsumCMS.Infrastructure.Validation.Internal
{

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    public class RuleBuilder<T, TResult> : 
        IPropertyRuleAware<T, TResult>,
        IRuleBuilderOptions
    {

        #region Fields

        private readonly PropertyRule<T, TResult> _rule;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rule"></param>
        public RuleBuilder(PropertyRule<T, TResult> rule)
        {
            _rule = rule;
        }

        #endregion

        #region Property Rule Aware

        /// <summary>
        /// 
        /// </summary>
        PropertyRule<T, TResult> IPropertyRuleAware<T, TResult>.Rule => _rule;

        #endregion

        #region Validate

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public RuleBuilder<T, TResult> Validate()
        {
            var validator = new ValidateUsingPropertyValidator<T, TResult, IValidator<TResult>>();
            _rule.AddValidator(validator);
            return this;
        }

        #endregion

        #region Validate Using

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TValidator"></typeparam>
        /// <returns></returns>
        public RuleBuilder<T, TResult> ValidateUsing<TValidator>()
            where TValidator : IValidator<TResult>
        {
            var validator = new ValidateUsingPropertyValidator<T, TResult, TValidator>();
            _rule.AddValidator(validator);
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="validateUsing"></param>
        /// <returns></returns>
        public RuleBuilder<T, TResult> ValidateUsing(Func<ValidationContext<TResult>, IEnumerable<ValidationMessage>> validateUsing)
        {
            var validator = new ValidateUsingPropertyValidator<T, TResult>(validateUsing);
            _rule.AddValidator(validator);
            return this;
        }

        #endregion

        #region Polymorphic Validate

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public RuleBuilder<T, TResult> PolymorphicValidate()
        {
            var validator = new PolymorphicValidatePropertyValidator<T, TResult>();
            _rule.AddValidator(validator);
            return this;
        }

        #endregion

        #region With Message

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public IRuleBuilderOptions WithMessage(string message)
        {
            _rule.SetCustomMessage(message);
            return this;
        }

        #endregion

    }

}

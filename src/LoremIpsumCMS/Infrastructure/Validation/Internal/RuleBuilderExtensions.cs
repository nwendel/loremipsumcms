﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Validation.PropertyValidators;

namespace LoremIpsumCMS.Infrastructure.Validation.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public static class RuleBuilderExtensions
    {

        #region Add Validator

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="self"></param>
        /// <param name="validator"></param>
        public static void AddValidator<T, TResult>(this RuleBuilder<T, TResult> self, IPropertyValidator<T, TResult> validator)
        {
            var ruleAware = (IPropertyRuleAware<T, TResult>)self;
            var rule = ruleAware.Rule;
            rule.AddValidator(validator);
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LoremIpsumCMS.Infrastructure.Extensions;
using LoremIpsumCMS.Infrastructure.Validation.PropertyValidators;

namespace LoremIpsumCMS.Infrastructure.Validation.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public class PropertyRule<T, TResult> : IPropertyRule<T>
    {

        #region Fields

        private readonly Func<T, TResult> _property;
        private readonly string _propertyName;
        private readonly IList<IPropertyValidator<T, TResult>> _propertyValidators = new List<IPropertyValidator<T, TResult>>();
        private string _customMessage;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyExpression"></param>
        public PropertyRule(Expression<Func<T, TResult>> propertyExpression)
        {
            _property = propertyExpression.Compile();
            _propertyName = propertyExpression.GetPropertyName();
        }

        #endregion

        #region Validate

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public IEnumerable<ValidationMessage> Validate(ValidationContext<T> context)
        {
            var value = _property(context.Instance);
            var messages = _propertyValidators.SelectMany(x => x.Validate(_propertyName, value, context.ServiceProvider));

            if (_customMessage != null && messages.Any())
            {
                return new[] { new ValidationMessage(_propertyName, _customMessage) };
            }

            return messages;
        }

        #endregion

        #region Add Validator

        /// <summary>
        /// 
        /// </summary>
        /// <param name="validator"></param>
        public void AddValidator(IPropertyValidator<T, TResult> validator)
        {
            _propertyValidators.Add(validator);
        }

        #endregion

        #region Set Custom Message

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customMessage"></param>
        public void SetCustomMessage(string customMessage)
        {
            _customMessage = customMessage;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Validation.Internal;
using LoremIpsumCMS.Infrastructure.Validation.PropertyValidators;

namespace LoremIpsumCMS.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public static class RuleBuilderExtensions
    {

        #region Default

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <returns></returns>
        public static RuleBuilder<T, TResult> Default<T, TResult>(this RuleBuilder<T, TResult> self)
        {
            var validator = new DefaultPropertyValidator<T, TResult>();
            self.AddValidator(validator);
            return self;
        }

        #endregion

        #region Not Default

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <returns></returns>
        public static RuleBuilder<T, TResult> NotDefault<T, TResult>(this RuleBuilder<T, TResult> self)
        {
            var validator = new NotDefaultPropertyValidator<T, TResult>();
            self.AddValidator(validator);
            return self;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using LoremIpsumCMS.Infrastructure.Validation.Internal;
using LoremIpsumCMS.Infrastructure.Validation.PropertyValidators;

namespace LoremIpsumCMS.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public static class RuleBuilderEnumerableExtensions
    {

        #region All

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="self"></param>
        /// <param name="itemValidatorAction"></param>
        /// <returns></returns>
        public static RuleBuilder<T, IEnumerable<TItem>> All<T, TItem>(this RuleBuilder<T, IEnumerable<TItem>> self, Action<RuleBuilder<TItem, TItem>> itemValidatorAction)
        {
            var validator = new EnumerableAllPropertyValidator<T, TItem>(itemValidatorAction);
            self.AddValidator(validator);
            return self;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="self"></param>
        /// <param name="itemValidatorAction"></param>
        /// <returns></returns>
        public static RuleBuilder<T, IEnumerable<TItem>> All<T, TItem>(this RuleBuilder<T, IEnumerable<TItem>> self, Action<InlineValidator<TItem>> itemValidatorAction)
        {
            var validator = new EnumerableAllInlinePropertyValidator<T, TItem>(itemValidatorAction);
            self.AddValidator(validator);
            return self;
        }

        #endregion

    }

}

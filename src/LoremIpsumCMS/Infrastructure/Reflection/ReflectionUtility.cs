﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.DotNet.PlatformAbstractions;
using Microsoft.Extensions.DependencyModel;

namespace LoremIpsumCMS.Infrastructure.Reflection
{

    /// <summary>
    /// 
    /// </summary>
    public static class ReflectionUtility
    {

        #region Find Application Assemblies

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Assembly> FindApplicationAssemblies()
        {
            var entryAssembly = Assembly.GetEntryAssembly();
            var applicationName = entryAssembly.GetRootNamespace();

            var assemblyNames = GetRuntimeAssemblyNames();
            var applicationAssemblyNames = assemblyNames.Where(x => x.IsInRootNamespace(applicationName)).ToList();

            return applicationAssemblyNames.Select(x => Assembly.Load(x)).ToList();
        }

        #endregion

        #region Find Lorem Ipsum Assemblies

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Assembly> FindLoremIpsumAssemblies()
        {
            // TODO: Is this correct?  Perhaps all which start with same names as modules?

            var assembly = typeof(LoremIpsumException).GetTypeInfo().Assembly;
            var name = assembly.GetRootNamespace();

            var assemblyNames = GetRuntimeAssemblyNames();
            var loremIpsumAssemblyNames = assemblyNames.Where(x => x.IsInRootNamespace(name)).ToList();

            return loremIpsumAssemblyNames.Select(x => Assembly.Load(x)).ToList();
        }

        #endregion

        #region Find Application and Lorem Ipsum Assemblies

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Assembly> FindApplicationAndLoremIpsumAssemblies()
        {
            var assemblies = FindApplicationAssemblies()
                .Concat(FindLoremIpsumAssemblies())
                .Distinct()
                .ToList();
            return assemblies;
        }

        #endregion

        #region Get Runtime Assembly Names

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<AssemblyName> GetRuntimeAssemblyNames()
        {
            var runtimeIdentifier = RuntimeEnvironment.GetRuntimeIdentifier();
            var assemblyNames = DependencyContext.Default.GetRuntimeAssemblyNames(runtimeIdentifier);
            return assemblyNames;
        }

        #endregion

        #region Root Namespace

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        private static string GetRootNamespace(this Assembly self)
        {
            var fullName = self.FullName;
            var index = fullName.IndexOfAny(new[] { '.', ',' });
            var rootNamespace = fullName.Substring(0, index);
            return rootNamespace;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="rootNamespace"></param>
        /// <returns></returns>
        private static bool IsInRootNamespace(this AssemblyName self, string rootNamespace)
        {
            return self.FullName.StartsWith(rootNamespace + ".") || self.FullName.StartsWith(rootNamespace + ",");
        }

        #endregion

    }

}

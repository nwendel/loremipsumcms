﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Raven.Client;
using LoremIpsumCMS.Infrastructure.Extensions;
using LoremIpsumCMS.Infrastructure.EventStore;
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Infrastructure.Validation;

namespace LoremIpsumCMS.Infrastructure.Cqrs
{

    /// <summary>
    /// 
    /// </summary>
    public class RavenCommandSender : ICommandSender
    {

        #region Dependencies

        private readonly ILogger _logger;
        private readonly IServiceProvider _serviceProvider;
        private readonly IValidationService _validationService;
        private readonly IEventStore _eventStore;
        private readonly IDocumentSession _documentSession;

        #endregion

        #region Fields

        private readonly ConcurrentDictionary<Type, Func<IEnumerable<AbstractEvent>, IEnumerable<AbstractEvent>>> _eventAppliers = new ConcurrentDictionary<Type, Func<IEnumerable<AbstractEvent>, IEnumerable<AbstractEvent>>>();
        private readonly ConcurrentDictionary<Type, bool> _shouldApplyEvent = new ConcurrentDictionary<Type, bool>();

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="serviceProvider"></param>
        /// <param name="validationService"></param>
        public RavenCommandSender(
            ILogger<RavenCommandSender> logger,
            IServiceProvider serviceProvider,
            IValidationService validationService,
            IEventStore eventStore,
            IDocumentSession documentSession)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
            _validationService = validationService;
            _eventStore = eventStore;
            _documentSession = documentSession;
        }

        #endregion

        #region Validate

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TCommand"></typeparam>
        /// <param name="command"></param>
        public void Validate<TCommand>(TCommand command)
            where TCommand : class, ICommand
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            var messages = _validationService.Validate(command);
            var message = messages.FirstOrDefault();
            if (message != null)
            {
                throw new ValidationException(message.Text);
            }

            var hasCommandvalidator = _serviceProvider.HasService<IValidateCommand<TCommand>>();
            if (hasCommandvalidator)
            {
                var commandValidator = _serviceProvider.GetService<IValidateCommand<TCommand>>();
                commandValidator.Validate(command);
            }
        }

        #endregion

        #region Send

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TCommand"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        public string Send<TCommand>(TCommand command)
            where TCommand : class, ICommand
        {
            _logger.LogDebug($"Sending command: {typeof(TCommand).FullName}");

            Validate(command);

            var events = HandleCommand(command);
            var applier = FindApplierForType(command.AggregateType);
            var appliedEvents = applier.Invoke(events).ToList();
            PublishEvents(appliedEvents);
            _eventStore.SaveEvents(appliedEvents);
            _documentSession.SaveChanges();

            var aggregateId = appliedEvents.FirstOrDefault()?.Id;
            return aggregateId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aggregateType"></param>
        /// <returns></returns>
        private Func<IEnumerable<AbstractEvent>, IEnumerable<AbstractEvent>> FindApplierForType(Type aggregateType)
        {
            Func<IEnumerable<AbstractEvent>, IEnumerable<AbstractEvent>> applier;

            var found = _eventAppliers.TryGetValue(aggregateType, out applier);
            if (found)
            {
                return applier;
            }

            var method = typeof(RavenCommandSender).GetTypeInfo().GetMethod(nameof(ApplyEvents));
            var genericMethod = method.MakeGenericMethod(new[] { aggregateType });
            applier = events =>
            {
                var result = genericMethod.InvokeAndUnwrapException(this, new object[] { events });
                return (IEnumerable<AbstractEvent>)result;
            };
            _eventAppliers[aggregateType] = applier;
            return applier;
        }

        #endregion

        #region Apply Events

        /// <summary>
        /// 
        /// </summary>
        /// <param name="events"></param>
        public IEnumerable<AbstractEvent> ApplyEvents<TAggregate>(IEnumerable<AbstractEvent> events)
            where TAggregate : AbstractAggregate, new()
        {
            var appliedEvents = new List<AbstractEvent>();
            AbstractAggregate aggregate = null;

            foreach (var @event in events)
            {
                if (aggregate == null)
                {
                    // TODO: Is this correct?
                    //       Only create aggregate if command doesn't have Id property?  
                    //       If it does the aggregate should already exist!
                    //       Perhaps IIdAware? On non-create commands?
                    aggregate = @event.Id == null
                        ? CreateAggregate<TAggregate>()
                        : LoadAggregate<TAggregate>(@event.Id);
                }

                if (@event.Id == null)
                {
                    @event.Id = aggregate.Id;
                }

                // TODO: By having the if statement here, it will load the aggregate even tho it is not used
                if (ApplyEventOnAggregate(@event))
                {
                    aggregate.ApplyEvent(@event);
                }
                appliedEvents.Add(@event);
            }
            return appliedEvents;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private TAggregate CreateAggregate<TAggregate>()
            where TAggregate : AbstractAggregate, new()
        {
            var aggregate = new TAggregate();
            _documentSession.Store(aggregate);
            return aggregate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TAggregate"></typeparam>
        /// <returns></returns>
        private TAggregate LoadAggregate<TAggregate>(string id)
            where TAggregate : AbstractAggregate
        {
            var aggregate = _documentSession.Load<TAggregate>(id);
            if (aggregate == null)
            {
                throw new CqrsException($"Failed to load aggregate with id {id}");
            }

            return aggregate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        /// <returns></returns>
        private bool ApplyEventOnAggregate(AbstractEvent @event)
        {
            var eventType = @event.GetType();

            bool shouldApply;
            var found = _shouldApplyEvent.TryGetValue(eventType, out shouldApply);
            if(found)
            {
                return shouldApply;
            }

            shouldApply = eventType.GetTypeInfo().GetCustomAttribute<SkipAggregateApplyAttribute>() == null;
            _shouldApplyEvent[eventType] = shouldApply;

            return shouldApply;
        }

        #endregion

        #region Handle Command

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TCommand"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        private IEnumerable<AbstractEvent> HandleCommand<TCommand>(TCommand command)
            where TCommand : ICommand
        {
            var commandHandler = _serviceProvider.GetRequiredService<IHandleCommand<TCommand>>();
            var events = commandHandler.Handle(command);
            return events;
        }

        #endregion

        #region Publish Events

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appliedEvents"></param>
        private void PublishEvents(IEnumerable<AbstractEvent> appliedEvents)
        {
            foreach (var appliedEvent in appliedEvents)
            {
                PublishEvent(appliedEvent);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appliedEvent"></param>
        private void PublishEvent(AbstractEvent appliedEvent)
        {
            var eventSubscriberType = typeof(ISubscribeTo<>).MakeGenericType(appliedEvent.GetType());
            var eventSubscribers = _serviceProvider.GetOptionalServices(eventSubscriberType);
            foreach (var eventSubscriber in eventSubscribers)
            {
                _logger.LogDebug($"Publishing event {appliedEvent.GetType().FullName} to {eventSubscriber.GetType().FullName}");

                var eventHandlerMethod = eventSubscriberType.GetMethod(nameof(ISubscribeTo<AbstractEvent>.Handle));
                eventHandlerMethod.InvokeAndUnwrapException(eventSubscriber, new object[] { appliedEvent });
            }
        }

        #endregion

    }

}

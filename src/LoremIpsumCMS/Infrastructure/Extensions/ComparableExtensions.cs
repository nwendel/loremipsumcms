﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;

namespace LoremIpsumCMS.Infrastructure.Extensions
{

    /// <summary>
    /// 
    /// </summary>
    public static class ComparableExtensions
    {

        #region Less Than

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static bool LessThan<T>(this IComparable<T> self, T other)
        {
            return self.CompareTo(other) < 0;
        }

        #endregion

        #region Less Than or Equal To

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static bool LessThanOrEqualTo<T>(this IComparable<T> self, T other)
        {
            return self.CompareTo(other) <= 0;
        }

        #endregion

        #region Greater Than

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static bool GreaterThan<T>(this IComparable<T> self, T other)
        {
            return self.CompareTo(other) > 0;
        }

        #endregion

        #region Greater Than or Equal To

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static bool GreaterThanOrEqualTo<T>(this IComparable<T> self, T other)
        {
            return self.CompareTo(other) >= 0;
        }

        #endregion

    }

}

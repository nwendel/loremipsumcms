﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;

namespace LoremIpsumCMS.Infrastructure.Extensions
{

    /// <summary>
    /// 
    /// </summary>
    public static class EnumerableExtensions
    {

        #region None

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <returns></returns>
        public static bool None<T>(this IEnumerable<T> self)
        {
            return !self.Any();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="self"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static bool None<TSource>(this IEnumerable<TSource> self, Func<TSource, bool> predicate)
        {
            return !self.Any(predicate);
        }

        #endregion

        #region One

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <returns></returns>
        public static bool One<T>(this IEnumerable<T> self)
        {
            if (self == null)
            {
                throw new ArgumentNullException(nameof(self));
            }

            using (var enumerator = self.GetEnumerator())
            {
                if(!enumerator.MoveNext())
                {
                    return false;
                }
                if(!enumerator.MoveNext())
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="self"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static bool One<TSource>(this IEnumerable<TSource> self, Func<TSource, bool> predicate)
        {
            if (self == null)
            {
                throw new ArgumentNullException(nameof(self));
            }
            if (predicate == null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }


            using (var enumerator = self.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    if (predicate(enumerator.Current))
                    {
                        while (enumerator.MoveNext())
                        {
                            if (predicate(enumerator.Current))
                            {
                                return false;
                            }
                        }
                        return true;
                    }
                }
            }
            return false;
        }

        #endregion

        #region Each

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <param name="action"></param>
        public static void Each<T>(this IEnumerable<T> self, Action<T> action)
        {
            if(self == null)
            {
                throw new ArgumentNullException(nameof(self));
            }
            if(action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            foreach(var item in self)
            {
                action(item);
            }
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Reflection;
using System.Runtime.ExceptionServices;

namespace LoremIpsumCMS.Infrastructure.Extensions
{

    /// <summary>
    /// 
    /// </summary>
    public static class MethodInfoExtensions
    {

        #region Invoke And Unwrap Exception

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="obj"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static object InvokeAndUnwrapException(this MethodInfo self, object obj, object[] parameters)
        {
            try
            {
                return self.Invoke(obj, parameters);
            }
            catch (TargetInvocationException ex)
            {
                ExceptionDispatchInfo.Capture(ex.InnerException).Throw();

                // This will never be called, but is needed to trick the compiler
                return null;
            }
        }

        #endregion

    }

}

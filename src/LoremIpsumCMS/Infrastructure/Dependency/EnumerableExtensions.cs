﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using LoremIpsumCMS.Infrastructure.Extensions;

namespace LoremIpsumCMS.Infrastructure.Dependency
{

    /// <summary>
    /// 
    /// </summary>
    public static class EnumerableExtensions
    {

        #region Order By Dependencies

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <returns></returns>
        public static IEnumerable<T> OrderByDependencies<T>(this IEnumerable<T> self)
        {
            var nodes = CreateDependencyNodes(self);
            CheckForInvalidDependency(nodes);

            var result = new Collection<T>();
            while (nodes.Any())
            {
                var nextNodes = nodes
                    .Where(x => x.DependsOn.None())
                    .ToList();

                if (nextNodes.None())
                {
                    var circle = FindCircle(nodes);
                    var circleText = string.Join(" -> ", circle.Select(x => x.FullName));
                    throw new DependencyException($"Found dependency circle: {circleText}");
                }

                nodes = nodes
                    .Where(x => x.DependsOn.Any())
                    .ToList();

                foreach (var nextItem in nextNodes)
                {
                    result.Add(nextItem.Instance);
                    foreach (var node in nodes)
                    {
                        var safeNextItem = nextItem;
                        node.DependsOn.RemoveWhere(x => x.Type == safeNextItem.Type);
                    }
                }
            }

            return result;
        }

        #endregion

        #region Create Dependency Nodes

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        private static IList<DependencyNode<T>> CreateDependencyNodes<T>(IEnumerable<T> items)
        {
            var nodes = items
                .Select(item => new DependencyNode<T>
                {
                    Type = item.GetType(),
                    Instance = item,
                    DependsOn = item.GetType().GetTypeInfo()
                        .GetCustomAttributes<DependsOnAttribute>(true)
                        .Select(t => new DependsOnNode { Type = t.Type, IsOptional = t.IsOptional })
                        .ToList()
                })
                .ToList();
            return nodes;
        }

        #endregion

        #region Check For Invalid Dependency

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="nodes"></param>
        private static void CheckForInvalidDependency<T>(IList<DependencyNode<T>> nodes)
        {
            var itemTypes = nodes
                .Select(x => x.Type)
                .ToList();

            foreach (var node in nodes)
            {
                node.DependsOn.RemoveWhere(x => x.IsOptional && !x.Type.In(itemTypes));

                foreach (var dependsOn in node.DependsOn)
                {
                    var dependsOnType = dependsOn.Type;
                    if (!dependsOnType.In(itemTypes))
                    {
                        throw new DependencyException($"{node.Type.FullName} depends on {dependsOn.Type.FullName} which cannot be found");
                    }
                }
            }
        }

        #endregion

        #region Find Circle

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="nodes"></param>
        /// <returns></returns>
        private static IEnumerable<Type> FindCircle<T>(IList<DependencyNode<T>> nodes)
        {
            var visited = Visit(nodes, new[] { nodes.First().Type });

            while(visited.First() != visited.Last())
            {
                visited.RemoveAt(0);
            }

            return visited;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="nodes"></param>
        /// <param name="visited"></param>
        /// <returns></returns>
        private static IList<Type> Visit<T>(IList<DependencyNode<T>> nodes, IList<Type> visited)
        {
            var lastNode = visited.Last();
            foreach (var next in nodes.First(x => x.Type == lastNode).DependsOn)
            {
                if(visited.Contains(next.Type))
                {
                    return visited.Concat(new[] { next.Type }).ToList();
                }
                
                var result = Visit(nodes, visited.Concat(new[] { next.Type }).ToList());
                if(result != null)
                {
                    return result;
                }
            }

            throw new ThisShouldNeverHappenException();
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Validation2.PropertyValidators;

namespace LoremIpsumCMS.Infrastructure.Validation2.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public class PropertyRuleBuilder<T, TResult> : AbstractRuleBuilder,
         IPropertyRuleBuilder<T, TResult>,
         IPropertyRuleAware<T, TResult>
    {

        #region Fields

        private readonly IPropertyRule<T, TResult> _rule;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rule"></param>
        public PropertyRuleBuilder(IPropertyRule<T, TResult> rule)
        {
            _rule = rule;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public IPropertyRule<T, TResult> Rule => _rule;

        #endregion

        #region Validate Using

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TValidator"></typeparam>
        /// <returns></returns>
        public IPropertyRuleBuilder<T, TResult> ValidateUsing<TValidator>() 
            where TValidator : IValidator<TResult>
        {
            var validator = new ValidateUsingPropertyValidator<T, TResult, TValidator>();
            _rule.AddValidator(validator);
            return this;
        }

        #endregion

        #region Polymorphic Validate

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IPropertyRuleBuilder<T, TResult> PolymorphicValidate()
        {
            var validator = new PolymorphicValidatePropertyValidator<T, TResult>();
            _rule.AddValidator(validator);
            return this;
        }

        #endregion

    }

}

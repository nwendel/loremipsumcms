﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
namespace LoremIpsumCMS.Infrastructure.Validation2.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public interface IPropertyRuleBuilder<T, out TResult> : IRuleBuilderOptions, IFluentInterface
    {

        #region Validate Using

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TValidator"></typeparam>
        /// <returns></returns>
        IPropertyRuleBuilder<T, TResult> ValidateUsing<TValidator>() 
            where TValidator : IValidator<TResult>;

        #endregion

        #region Polymorphic Validate

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IPropertyRuleBuilder<T, TResult> PolymorphicValidate();

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Validation2.Internal;
using LoremIpsumCMS.Infrastructure.Validation2.PropertyValidators;

namespace LoremIpsumCMS.Infrastructure.Validation2
{

    /// <summary>
    /// 
    /// </summary>
    public static class RuleBuilderReferenceTypeExtensions
    {

        #region Null

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <returns></returns>
        public static IPropertyRuleBuilder<T, TResult> Null<T, TResult>(this IPropertyRuleBuilder<T, TResult> self)
            where TResult : class
        {
            var validator = new NullPropertyValidator<T, TResult>();
            self.AddValidator(validator);
            return self;
        }

        #endregion

        #region Not Null

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <returns></returns>
        public static IPropertyRuleBuilder<T, TResult> NotNull<T, TResult>(this IPropertyRuleBuilder<T, TResult> self)
            where TResult : class
        {
            var validator = new NotNullPropertyValidator<T, TResult>();
            self.AddValidator(validator);
            return self;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LoremIpsumCMS.Infrastructure.Validation2.Internal;

namespace LoremIpsumCMS.Infrastructure.Validation2
{
    /// <summary>
    /// 
    /// </summary>
    public class AbstractValidator<T> : IValidator<T>
    {

        #region Fields

        private readonly IList<IRule<T>> _rules = new List<IRule<T>>();

        #endregion

        #region Validate

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        IEnumerable<ValidationMessage> IValidator<T>.Validate(IValidationContext<T> context)
        {
            return Validate((ValidationContext<T>)context);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public IEnumerable<ValidationMessage> Validate(ValidationContext<T> context)
        {
            var messages = _rules.SelectMany(x => x.Validate(context));
            return messages;
        }

        #endregion

        #region Property

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="propertyExpression"></param>
        /// <returns></returns>
        protected IPropertyRuleBuilder<T, TResult> Property<TResult>(Expression<Func<T, TResult>> propertyExpression)
        {
            var rule = new PropertyRule<T, TResult>(propertyExpression);
            _rules.Add(rule);
            var builder = new PropertyRuleBuilder<T, TResult>(rule);
            return builder;
        }

        #endregion

        #region Validate using

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TValidator"></typeparam>
        protected IRuleBuilderOptions ValidateUsing<TValidator>()
            where TValidator : IValidator<T>
        {
            var rule = new InstanceRule<T>();
            _rules.Add(rule);
            var builder = new InstanceRuleBuilder<T>(rule);
            return builder;
        }

        #endregion

    }

}

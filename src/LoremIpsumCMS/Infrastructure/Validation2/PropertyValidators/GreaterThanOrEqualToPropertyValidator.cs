﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using LoremIpsumCMS.Infrastructure.Extensions;

namespace LoremIpsumCMS.Infrastructure.Validation2.PropertyValidators
{

    /// <summary>
    /// 
    /// </summary>
    public class GreaterThanOrEqualToPropertyValidator<T, TResult> : IPropertyValidator<TResult>
        where TResult : IComparable<TResult>
    {

        #region Fields

        private readonly TResult _greaterThanOrEqualTo;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="greaterThanOrEqualTo"></param>
        public GreaterThanOrEqualToPropertyValidator(TResult greaterThanOrEqualTo)
        {
            _greaterThanOrEqualTo = greaterThanOrEqualTo;
        }

        #endregion

        #region Validate

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public IEnumerable<ValidationMessage> Validate(string propertyName, TResult value, IServiceProvider serviceProvider)
        {
            if (value == null)
            {
                yield break;
            }

            if (value.LessThan(_greaterThanOrEqualTo))
            {
                yield return new ValidationMessage(propertyName, $"{propertyName} must be {_greaterThanOrEqualTo} or greater");
            }
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace LoremIpsumCMS.Infrastructure.Validation2.PropertyValidators
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidateUsingPropertyValidator<T, TResult, TValidator> : IPropertyValidator<TResult>
        where TValidator : IValidator<TResult>
    {

        #region Validate

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        public IEnumerable<ValidationMessage> Validate(string propertyName, TResult value, IServiceProvider serviceProvider)
        {
            if (value == null)
            {
                return new ValidationMessage[0];
            }

            var context = new ValidationContext<TResult>(value, serviceProvider);
            var validators = serviceProvider.GetServices<TValidator>();
            var messages = validators.SelectMany(validator =>
            {
                var validatorMessages = validator.Validate(context);
                var adjustedValidatorMessages = validatorMessages.Select(x => new ValidationMessage($"{propertyName}.{x.PropertyName}", x.Text));
                return adjustedValidatorMessages;
            });

            return messages;
        }

        #endregion

    }

}

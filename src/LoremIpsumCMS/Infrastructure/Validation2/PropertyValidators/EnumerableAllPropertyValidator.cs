﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using LoremIpsumCMS.Infrastructure.Validation2.Internal;

namespace LoremIpsumCMS.Infrastructure.Validation2.PropertyValidators
{

    /// <summary>
    /// 
    /// </summary>
    public class EnumerableAllPropertyValidator<T, TResult> : IPropertyValidator<IEnumerable<TResult>>
    {

        private readonly Action<IPropertyRuleBuilder<object, TResult>> _itemValidationAction;
        private readonly IRule<TResult> _rule;

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemValidationAction"></param>
        public EnumerableAllPropertyValidator(Action<IPropertyRuleBuilder<object, TResult>> itemValidationAction)
        {
            _itemValidationAction = itemValidationAction;
            _rule = new InstanceRule<TResult>();

            //_itemValidationAction(new InstanceRuleBuilder<TResult>(_rule));
        }

        #endregion

        #region Validate

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        public IEnumerable<ValidationMessage> Validate(string propertyName, IEnumerable<TResult> value, IServiceProvider serviceProvider)
        {
            if(value == null)
            {
                return new ValidationMessage[0];
            }

            var messages = value.SelectMany((item, ix) =>
            {
                var context = new ValidationContext<TResult>(item, serviceProvider);
                var itemMessages = _rule.Validate(context);

                // TODO: Is this correct, need to think about it
                var adjustedItemMessages = itemMessages.Select(x => x.PropertyName == string.Empty
                    ? new ValidationMessage($"{propertyName}[{ix}]", $"Values for {propertyName}{x.Text}")
                    : new ValidationMessage($"{propertyName}[{ix}]{x.PropertyName}", x.Text));
                return adjustedItemMessages;
            });
            return messages;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using LoremIpsumCMS.Infrastructure.Validation2.Internal;
using LoremIpsumCMS.Infrastructure.Validation2.PropertyValidators;

namespace LoremIpsumCMS.Infrastructure.Validation2
{

    /// <summary>
    /// 
    /// </summary>
    public static class RuleBuilderComparableExtensions
    {

        #region Less Than

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="self"></param>
        /// <param name="lessThan"></param>
        /// <returns></returns>
        public static IPropertyRuleBuilder<T, TResult> LessThan<T, TResult>(this IPropertyRuleBuilder<T, TResult> self, TResult lessThan)
            where TResult : IComparable<TResult>
        {
            var validator = new LessThanPropertyValidator<T, TResult>(lessThan);
            self.AddValidator(validator);
            return self;
        }

        #endregion

        #region Less Than or Equal To

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="self"></param>
        /// <param name="lessThanOrEqualTo"></param>
        /// <returns></returns>
        public static IPropertyRuleBuilder<T, TResult> LessThanOrEqualTo<T, TResult>(this IPropertyRuleBuilder<T, TResult> self, TResult lessThanOrEqualTo)
            where TResult : IComparable<TResult>
        {
            var validator = new LessThanOrEqualPropertyValidator<T, TResult>(lessThanOrEqualTo);
            self.AddValidator(validator);
            return self;
        }

        #endregion

        #region Greater Than

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="self"></param>
        /// <param name="greaterThan"></param>
        /// <returns></returns>
        public static IPropertyRuleBuilder<T, TResult> GreaterThan<T, TResult>(this IPropertyRuleBuilder<T, TResult> self, TResult greaterThan)
            where TResult : IComparable<TResult>
        {
            var validator = new GreaterThanPropertyValidator<T, TResult>(greaterThan);
            self.AddValidator(validator);
            return self;
        }

        #endregion

        #region Greater Than or Equal To

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="self"></param>
        /// <param name="greaterThanOrEqualTo"></param>
        /// <returns></returns>
        public static IPropertyRuleBuilder<T, TResult> GreaterThanOrEqualTo<T, TResult>(this IPropertyRuleBuilder<T, TResult> self, TResult greaterThanOrEqualTo)
            where TResult : IComparable<TResult>
        {
            var validator = new GreaterThanOrEqualToPropertyValidator<T, TResult>(greaterThanOrEqualTo);
            self.AddValidator(validator);
            return self;
        }

        #endregion

    }

}

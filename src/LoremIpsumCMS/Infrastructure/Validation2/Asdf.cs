﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoremIpsumCMS.Infrastructure.Validation2
{

    /// <summary>
    /// 
    /// </summary>
    public class Asdf : AbstractValidator<Asdf>
    {

        public int p1 { get; set; }

        public string p2 { get; set; }

        public IEnumerable<string> p3 { get; set; }

        public IList<string> p4 { get; set; }

        public IEnumerable<Asdf> p5 { get; set; }

        public IList<Asdf> p6 { get; set; }

        public Asdf p7 { get; set; }

        public Asdf()
        {
            Property(x => x.p1).NotDefault();
            Property(x => x.p2).NotNull();

            Property(x => x.p3).All(x => x.NotNull());
            Property(x => x.p4).All(x => x.NotNull());

            Property(x => x.p5).All(e =>
            {
                e.Property(x => x.p2).NotNull();
            });
            Property(x => x.p6).All(e =>
            {
                e.Property(x => x.p2).NotNull();
            });

            Property(x => x.p7).ValidateUsing<Asdf>();

            ValidateUsing<Asdf>();

        }

    }

}

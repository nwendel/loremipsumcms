﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq.Expressions;
using LoremIpsumCMS.Infrastructure.Extensions;

namespace LoremIpsumCMS.Infrastructure.Validation2
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidationMessage
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="text"></param>
        /// <param name="args"></param>
        public ValidationMessage(string propertyName, string text, params object[] args)
        {
            if (text == null)
            {
                throw new ArgumentNullException(nameof(text));
            }
            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(propertyName));
            }

            PropertyName = propertyName;
            Text = string.Format(text, args);
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string PropertyName { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public string Text { get; private set; }

        #endregion

    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ValidationMessage<T> : ValidationMessage
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="text"></param>
        /// <param name="args"></param>
        public ValidationMessage(Expression<Func<T, object>> propertyExpression, string text, params object[] args)
            : base(propertyExpression.GetPropertyName(), text, args)
        {
        }

        #endregion

    }

}
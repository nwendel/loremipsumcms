﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq.Expressions;

namespace LoremIpsumCMS.Infrastructure.Validation2
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidationContext<T> : IValidationContext<T>
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="serviceProvider"></param>
        public ValidationContext(T instance, IServiceProvider serviceProvider)
        {
            Instance = instance;
            ServiceProvider = serviceProvider;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public T Instance { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public IServiceProvider ServiceProvider { get; private set; }

        #endregion

        #region Is Valid

        // TODO: These function are not defined on IValidationContext since T is covariant, which makes it impossible to use the Expression<Func<>> parameter

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            throw new NotImplementedException();
            //return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="propertyExpression"></param>
        /// <returns></returns>
        public bool IsValid<TResult>(Expression<Func<T, TResult>> propertyExpression)
        {
            throw new NotImplementedException();
            //return true;
        }

        #endregion

    }

}

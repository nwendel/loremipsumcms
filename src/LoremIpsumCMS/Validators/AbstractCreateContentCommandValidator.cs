﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Model;

namespace LoremIpsumCMS.Validators
{

    /// <summary>
    /// 
    /// </summary>
    public class AbstractCreateContentCommandValidator<TCommand, T, TData, TExtension> : AbstractValidator<TCommand>
        where TCommand : AbstractCreateContentCommand<T, TData, TExtension>
        where T : AbstractAggregate
        where TData : AbstractContentData
        where TExtension : AbstractExtensionProperties
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public AbstractCreateContentCommandValidator()
        {
            Property(x => x.SiteId)
                .NotNull();
            Property(x => x.Data)
                .NotNull()
                .PolymorphicValidate();
            Property(x => x.ExtensionProperties)
                .NotNull()
                .All(x => x
                    .NotNull()
                    .PolymorphicValidate());
            Property(x => x.CreatedAt).NotDefault();
        }

        #endregion

    }

}

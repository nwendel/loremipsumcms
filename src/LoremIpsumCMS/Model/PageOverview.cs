﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Model;
using Raven.Client.UniqueConstraints;

namespace LoremIpsumCMS.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class PageOverview : AbstractReadModel
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        protected PageOverview()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="siteId"></param>
        /// <param name="path"></param>
        /// <param name="title"></param>
        /// <param name="dataTypeFullName"></param>
        public PageOverview(string pageId, string siteId, string path, string title, string dataTypeFullName)
        {
            PageId = pageId;
            SiteId = siteId;
            Path = path;
            Title = title;
            DataTypeFullName = dataTypeFullName;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [UniqueConstraint]
        public string PageId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string SiteId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string Path { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        [UniqueConstraint]
        public string SiteIdAndPath => $"{SiteId}/{Path}";

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string DataTypeFullName { get; protected set; }

        #endregion

        #region Update

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="title"></param>
        public void Update(string path, string title)
        {
            Path = path;
            Title = title;
        }

        #endregion

    }

}

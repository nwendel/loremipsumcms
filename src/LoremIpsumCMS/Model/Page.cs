﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Raven.Client.UniqueConstraints;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Cqrs;

namespace LoremIpsumCMS.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class Page : AbstractContent<AbstractPageData, AbstractPageExtensionProperties>,
        IApplyEvent<PageCreatedEvent>,
        IApplyEvent<PageUpdatedEvent>
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string Path { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        [UniqueConstraint]
        public string SiteIdAndPath => $"{SiteId}/{Path}";

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MetaKeywords { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string MetaDescription { get; protected set; }

        #endregion

        #region Apply Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(PageCreatedEvent @event)
        {
            Path = @event.Path;
            Title = @event.Title;
            MetaKeywords = @event.MetaKeywords;
            MetaDescription = @event.MetaDescription;
            base.Apply(@event);
        }

        #endregion

        #region Apply Updated

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(PageUpdatedEvent @event)
        {
            Path = @event.Path;
            Title = @event.Title;
            MetaKeywords = @event.MetaKeywords;
            MetaDescription = @event.MetaDescription;
            base.Apply(@event);
        }

        #endregion

    }

}

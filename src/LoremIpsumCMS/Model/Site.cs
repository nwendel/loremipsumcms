﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Raven.Client.UniqueConstraints;
using Raven.Imports.Newtonsoft.Json;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Extensions;
using LoremIpsumCMS.Infrastructure.Model;

namespace LoremIpsumCMS.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class Site : AbstractAggregate, IExtensionPropertiesAware<AbstractSiteExtensionProperties>,
        IApplyEvent<SiteCreatedEvent>,
        IApplyEvent<SiteUpdatedEvent>
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public Site()
        {
            ExtensionPropertiesInner = new Collection<AbstractSiteExtensionProperties>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        [UniqueConstraint]
        public string NameSlug => Name.Slugify();

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<SiteHost> Hosts { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        protected IList<AbstractSiteExtensionProperties> ExtensionPropertiesInner { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public IEnumerable<AbstractSiteExtensionProperties> ExtensionProperties => new ReadOnlyCollection<AbstractSiteExtensionProperties>(ExtensionPropertiesInner);

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedAt { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastUpdatedAt { get; protected set; }

        #endregion

        #region Apply Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(SiteCreatedEvent @event)
        {
            Name = @event.Name;
            Hosts = @event.Hosts;
            ExtensionPropertiesInner = @event.ExtensionProperties.ToList();
            CreatedAt = @event.CreatedAt;
        }

        #endregion

        #region Apply Updated

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(SiteUpdatedEvent @event)
        {
            Name = @event.Name;
            Hosts = @event.Hosts;
            ExtensionPropertiesInner = @event.ExtensionProperties.ToList();
            LastUpdatedAt = @event.UpdatedAt;
        }

        #endregion

    }

}

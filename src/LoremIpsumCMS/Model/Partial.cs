﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Cqrs;

namespace LoremIpsumCMS.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class Partial : AbstractContent<AbstractPartialData, AbstractPartialExtensionProperties>,
        IApplyEvent<PartialCreatedEvent>,
        IApplyEvent<PartialUpdatedEvent>
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; protected set; }

        #endregion

        #region Apply Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(PartialCreatedEvent @event)
        {
            Name = @event.Name;
            base.Apply(@event);
        }

        #endregion

        #region Apply Updated

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(PartialUpdatedEvent @event)
        {
            Name = @event.Name;
            base.Apply(@event);
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Raven.Client;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;

namespace LoremIpsumCMS.EventHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class PageOverviewEventHandler :
        ISubscribeTo<PageCreatedEvent>,
        ISubscribeTo<PageUpdatedEvent>,
        ISubscribeTo<PageDeletedEvent>
    {

        #region Dependencies

        private readonly IDocumentSession _documentSesion;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        public PageOverviewEventHandler(IDocumentSession documentSession)
        {
            _documentSesion = documentSession;
        }

        #endregion

        #region Handle Page Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Handle(PageCreatedEvent @event)
        {
            var pageOverview = new PageOverview(
                @event.Id, 
                @event.SiteId, 
                @event.Path, 
                @event.Title, 
                @event.Data.GetType().FullName);
            _documentSesion.Store(pageOverview);
        }

        #endregion

        #region Handle Page Updated

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Handle(PageUpdatedEvent @event)
        {
            var pageOverview = _documentSesion.LoadBy<PageOverview>(x => x.ByPageId(@event.Id));
            pageOverview.Update(@event.Path, @event.Title);
        }

        #endregion

        #region Handle Page Deleted

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Handle(PageDeletedEvent @event)
        {
            var pageOverview = _documentSesion.LoadBy<PageOverview>(x => x.ByPageId(@event.Id));
            _documentSesion.Delete(pageOverview);
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2016
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using Raven.Client;
using Raven.Client.Linq;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Model;

namespace LoremIpsumCMS.EventHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class HostEventHandler :
        ISubscribeTo<SiteCreatedEvent>,
        ISubscribeTo<SiteUpdatedEvent>
    {

        #region Dependencies

        private readonly IDocumentSession _documentSesion;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        public HostEventHandler(IDocumentSession documentSession)
        {
            _documentSesion = documentSession;
        }

        #endregion

        #region Handle Site Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Handle(SiteCreatedEvent @event)
        {
            foreach(var siteHost in @event.Hosts)
            {
                var host = new Host(siteHost.Name, @event.Id);
                _documentSesion.Store(host);
            }
        }

        #endregion

        #region Handle Site Updated

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Handle(SiteUpdatedEvent @event)
        {
            // TODO: Some other way to do bulk delete?
            var oldHosts = _documentSesion.Query<Host>()
                .Customize(o => o.WaitForNonStaleResults())
                .Where(x => x.SiteId == @event.Id)
                .ToList();
            foreach (var oldHost in oldHosts)
            {
                _documentSesion.Delete(oldHost);
            }

            foreach (var siteHost in @event.Hosts)
            {
                var host = new Host(siteHost.Name, @event.Id);
                _documentSesion.Store(host);
            }
        }

        #endregion

    }

}
